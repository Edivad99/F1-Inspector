﻿using F1_Inspector.Class;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class NewsArticle : Page
    {
        ObservableCollection<Articolo> lista = new ObservableCollection<Articolo>();
        ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView();
        Articolo articoloPrecendete;

        public NewsArticle()
        {
            this.InitializeComponent();
            Paragrafo.Text = "";
            foto.Source = null;

            //if (NetworkInterface.GetIsNetworkAvailable())
            //{
            //    pubblicita.Visibility = Visibility.Visible;
            //    pubblicita.IsAutoRefreshEnabled = true;
            //    pubblicita.ApplicationId = "9nthjhlp8f0b";
            //    pubblicita.AdUnitId = "1100026076";
            //}
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var listaRAW = ((ArticoliParameter)e.Parameter).lista;
            var articolo = listaRAW[((ArticoliParameter)e.Parameter).indice];
            Load(listaRAW, articolo);
        }

        public void Load(ObservableCollection<Articolo> listaRAW, Articolo articolo)
        {
            //Inizializzo tutto
            Titolo.Visibility = Subtitolo.Visibility = Autore.Visibility = TempoLettura.Visibility = Link.Visibility = Visibility.Visible;

            try
            {
                if (articolo is F1GrandPrix)
                {
                    F1GrandPrixViewer((F1GrandPrix)articolo);
                }
                else if (articolo is FormulaPassion)
                {
                    FormualaPassionViewer((FormulaPassion)articolo);
                }
                else if (articolo is MotorSport)
                {
                    MotorSportViewer((MotorSport)articolo);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Errore News.cs: " + e.Message);
                var m = new MessageDialog(resourceLoader.GetString("qualcosaStorto")).ShowAsync();
            }


            if (lista.Count > 0)
            {
                lista.Insert(lista.Count, articoloPrecendete);
                lista.Remove(articolo);
            }
            else
            {
                foreach (var x in listaRAW)
                    if (x.Titolo != articolo.Titolo && x.Giornale == articolo.Giornale)
                        lista.Add(x);


            }
            articoloPrecendete = articolo;
        }

        public async void F1GrandPrixViewer(F1GrandPrix Articolo)
        {
            load.IsActive = true;
            var client = new HttpClient();
            var responseArray = await client.GetByteArrayAsync(Articolo.Url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            //Recupero il titolo
            Titolo.Text = Articolo.Titolo;

            //Recupero il subtitolo
            var subtitoloRAW = doc.DocumentNode.SelectSingleNode("//header[@class='head']//h2");
            if (subtitoloRAW != null)
            {
                Subtitolo.Visibility = Visibility.Visible;
                Subtitolo.Text = System.Net.WebUtility.HtmlDecode(subtitoloRAW.InnerText).Trim();
            }
            else
                Subtitolo.Visibility = Visibility.Collapsed;

            //Recupero la foto
            foto.Source = Articolo.Foto;

            //Recupero autore e data pubblicazione
            var dataRaw = doc.DocumentNode.SelectSingleNode("//div[@class='post-info']//span[@class='date']").InnerText;
            Autore.Text = string.Format("di {0} ({1}) il {2}", Articolo.Autore, Articolo.Giornale, dataRaw);

            Link.NavigateUri = Articolo.Url;

            //Recupero il paragrafo
            var paragrafoRAW = doc.DocumentNode.SelectNodes("//div[@class='contentArticleSingle']//p");
            string risposta = "";
            string parentNode_XPath = "";
            if (paragrafoRAW != null)
            {
                for (int i = 0; i < paragrafoRAW.Count; i++)
                {
                    if (i == 0)
                    {
                        parentNode_XPath = paragrafoRAW[i].ParentNode.XPath + "/" + paragrafoRAW[i].OriginalName;
                    }

                    if (paragrafoRAW[i].XPath.Contains(parentNode_XPath))
                    {
                        if(!paragrafoRAW[i].InnerText.Contains("getElementsByTagName"))
                            risposta += paragrafoRAW[i].InnerText + "\n";
                    }
                }
                if (risposta.Length > 0)
                    risposta = risposta.Substring(0, risposta.Length - 1);

                response = System.Net.WebUtility.HtmlDecode(risposta.Replace(Articolo.Autore, "").Trim());
                TempoLettura.Text = resourceLoader.GetString("ReadTime") + ArticoliUtils.calcoloTempoLettura(response);
                Paragrafo.Text = response;
            }
            else
            {
                Paragrafo.Text = "";
                TempoLettura.Text = resourceLoader.GetString("ReadTime") + "indefinito";
                var m = new MessageDialog(resourceLoader.GetString("erroreTesto")).ShowAsync();
            }

            load.IsActive = false;
        }

        public async void FormualaPassionViewer(FormulaPassion Articolo)
        {
            load.IsActive = true;
            var client = new HttpClient();
            var responseArray = await client.GetByteArrayAsync(Articolo.Url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            //Recupero il titolo
            Titolo.Text = Articolo.Titolo;

            //Recupero il subtitolo
            var subtitoloRAW = doc.DocumentNode.SelectSingleNode("//div[@class='single-riassunto']//h2");
            if (subtitoloRAW != null)
            {
                Subtitolo.Visibility = Visibility.Visible;
                Subtitolo.Text = System.Net.WebUtility.HtmlDecode(subtitoloRAW.InnerText).Trim();
            }
            else
                Subtitolo.Visibility = Visibility.Collapsed;


            //Recupero la foto
            foto.Source = Articolo.Foto;

            //Recupero autore e data pubblicazione
            var dataRaw = doc.DocumentNode.SelectSingleNode("//div[@class='col-md-5 text-right entry-meta']//time[@class='entry-date published']").InnerText.Split(' ');
            var autoreRaw = doc.DocumentNode.SelectSingleNode("//div[@class='author-link']").InnerText.Trim();
            //Sistemo il nome dell'autore
            string oldStr = autoreRaw.Substring(5).Replace("\r\n", string.Empty).Replace("\t", string.Empty);
            string newStr = Regex.Replace(oldStr, " {2,}", " ");
            Articolo.Autore = newStr;

            var data = string.Format("{0} {1}, {2}", dataRaw[0], dataRaw[1], dataRaw[2]);
            Autore.Text = string.Format("di {0} ({1}) il {2}", Articolo.Autore, Articolo.Giornale, data);

            Link.NavigateUri = Articolo.Url;

            //Recupero il paragrafo
            var paragrafoRAW = doc.DocumentNode.SelectNodes("//div[@class='entry-content']//p");
            string risposta = "";

            if (paragrafoRAW != null)
            {
                for (int i = 0; i < paragrafoRAW.Count; i++)
                {
                    string s = System.Net.WebUtility.HtmlDecode(paragrafoRAW[i].InnerText).Trim();
                    if (s.Length > 0)// se la lunghezza non è più grande di 0, vuol dire che non sono nei tag p che contegono testo
                    {
                        risposta += s + "\n";
                    }
                }
                if (risposta.Length > 0)
                    risposta = risposta.Substring(0, risposta.Length - 1);
                else
                {
                    var m = new MessageDialog(resourceLoader.GetString("elementiNonsupportati")).ShowAsync();
                }

                TempoLettura.Text = resourceLoader.GetString("ReadTime") + ArticoliUtils.calcoloTempoLettura(risposta);
                Paragrafo.Text = risposta;
            }
            else
            {
                Paragrafo.Text = "";
                TempoLettura.Text = resourceLoader.GetString("ReadTime") + "indefinito";
                var m = new MessageDialog(resourceLoader.GetString("erroreTesto")).ShowAsync();
            }
            load.IsActive = false;
        }

        public async void MotorSportViewer(MotorSport Articolo)
        {
            load.IsActive = true;
            var client = new HttpClient();
            var responseArray = await client.GetByteArrayAsync(Articolo.Url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            //Recupero il titolo
            Titolo.Text = Articolo.Titolo;

            //Recupero il subtitolo
            var subtitoloRAW = doc.DocumentNode.SelectSingleNode("//h2[@class='ms-article_preview ms-mb']").InnerText;
            Subtitolo.Text = System.Net.WebUtility.HtmlDecode(subtitoloRAW).Trim();

            //Recupero la foto
            foto.Source = Articolo.Foto;

            //Recupero autore e data pubblicazione
            try
            {
                var autoreRaw = doc.DocumentNode.SelectSingleNode("//address[@class='ms-article-subheader_author-wrapper']//a[@rel='author']").InnerText.Trim();
                Articolo.Autore = autoreRaw;
                Autore.Text = string.Format("di {0} ({1})", Articolo.Autore, Articolo.Giornale);
            }
            catch
            {
                Autore.Visibility = Visibility.Collapsed;
            }
            
            Link.NavigateUri = Articolo.Url;

            //Recupero il paragrafo
            var paragrafoRAW = doc.DocumentNode.SelectNodes("//div[@class='ms-article-content']//p");
            if (paragrafoRAW != null)
            {
                string risposta = "";

                for (int i = 0; i < paragrafoRAW.Count; i++)
                {
                    string s = System.Net.WebUtility.HtmlDecode(paragrafoRAW[i].InnerText).Trim();
                    if (s.Length > 0)// se la lunghezza non è più grande di 0, vuol dire che non sono nei tag p che contegono testo
                    {
                        string lower = s.ToLower();
                        if (!lower.Contains("foto di:") && !lower.Contains("photo by:"))
                            risposta += s + "\n";
                    }
                }
                if (risposta.Length > 0)
                    risposta = risposta.Substring(0, risposta.Length - 1);
                else
                {
                    var m = new MessageDialog(resourceLoader.GetString("elementiNonsupportati")).ShowAsync();
                }

                TempoLettura.Text = resourceLoader.GetString("ReadTime") + ArticoliUtils.calcoloTempoLettura(risposta);
                Paragrafo.Text = risposta;
            }
            else
            {
                Paragrafo.Text = "";
                TempoLettura.Text = resourceLoader.GetString("ReadTime") + "indefinito";
                //var m = new MessageDialog("In questo articolo sono presenti elementi, che attualmente non supportiamo").ShowAsync();
                var m = new MessageDialog(resourceLoader.GetString("erroreTesto")).ShowAsync();
            }

            load.IsActive = false;
        }

        private void articoli_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var i = articoli2.SelectedIndex;
            if (i != -1)
            {
                var Articolo = lista[i];
                Load(lista, Articolo);
            }
        }

        //private void pubblicita_ErrorOccurred(object sender, Microsoft.Advertising.WinRT.UI.AdErrorEventArgs e)
        //{
        //    Debug.WriteLine("Errore AD: " + e.ErrorMessage);
        //}
    }
}
