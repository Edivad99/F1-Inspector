﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Services.Maps;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Circuiti
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class CircuitiDetails : Page
    {
        ResourceLoader resourceLoader;
        public CircuitiDetails()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }
        Race corsa;
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            load.IsActive = true;
            bool errore = false;

            corsa = e.Parameter as Race;

            //Statistiche Circuito
            CircuitName.Text = corsa.raceName;
            TimeGranPrix.Text += corsa.getLocalTime();
            var Statistiche = await corsa.Circuit.Statistiche();

            if (Statistiche != null)
            {
                FirstGranPrix.Text += Statistiche["FIRST GRAND PRIX"];
                NumberLaps.Text += Statistiche["NUMBER OF LAPS"];
                CircuitLength.Text += Statistiche["CIRCUIT LENGTH"];
                RaceDistance.Text += Statistiche["RACE DISTANCE"];
                LapRecord.Text += Statistiche["LAP RECORD"];

                if (corsa.isComplete())
                {
                    ClassificaGP.Visibility = Visibility.Visible;
                    DocumentiFIA.Visibility = Visibility.Visible;
                }
                    

                try
                {
                    //Immagine della pista
                    var immagine = new BitmapImage(new Uri(Statistiche["Track image"], UriKind.Absolute));
                    foto.Source = immagine;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Errore download immagine pista: " + ex.Message);
                    errore = true;
                }
            }
            else
            {
                errore = true;
            }

            if(errore)
            {
                load.IsActive = false;
                var m = new MessageDialog(resourceLoader.GetString("EIC"));
                m.Commands.Add(new UICommand("OK") { Id = 0 });
                m.DefaultCommandIndex = 0;
                var result = await m.ShowAsync();
                if ((int)result.Id == 0)
                {
                    if (this.Frame.CanGoBack)
                        this.Frame.GoBack();
                }
            }
            else
            {
                load.IsActive = false;
                load.Visibility = Visibility.Collapsed;
            }
            
        }
        

        private async void ImpostaMappa (Circuit circuito)
        {
            // Center on track
            var uriTrack = new Uri(string.Format("bingmaps:?collection=point.{0}_{1}_{2}&lvl=10", circuito.Location.lat, circuito.Location.@long, circuito.circuitName));

            // Launch the Windows Maps app
            var launcherOptions = new Windows.System.LauncherOptions();
            launcherOptions.TargetApplicationPackageFamilyName = "Microsoft.WindowsMaps_8wekyb3d8bbwe";
            var success = await Windows.System.Launcher.LaunchUriAsync(uriTrack, launcherOptions);
        }

        private void AvviaMaps_Click(object sender, RoutedEventArgs e)
        {
            ImpostaMappa(corsa.Circuit);
        }

        private void ClassificaGP_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Risultati.RisultatiDetails), corsa.round);
        }

        private void DocumentiFIA_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
