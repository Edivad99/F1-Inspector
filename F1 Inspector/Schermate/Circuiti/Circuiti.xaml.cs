﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Circuiti
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Circuiti : Page
    {
        ResourceLoader resourceLoader;
        public Circuiti()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            MostraCircuiti();
        }

        public async void MostraCircuiti()
        {
            load.IsActive = true;

            GetData gd = new GetData();
            var circuiti = await gd.getCircuiti();
            if (circuiti != null)
            {
                foreach (var circuito in circuiti)
                {
                    CircuitiView.Items.Add(circuito);
                    Debug.WriteLine(circuito.raceName + ";" + circuito.getLocalTime() + ";" + circuito.Circuit.Location.country);
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

            load.IsActive = false;
        }

        private void CircuitiView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var circuito = CircuitiView.SelectedItem;
            Frame.Navigate(typeof(CircuitiDetails), circuito);
        }
    }
}
