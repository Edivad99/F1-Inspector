﻿using F1_Inspector.Class;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Replay2 : Page
    {
        ResourceLoader resourceLoader;
        List<Race> circuiti;
        Circuit circuitoSelezionato;
        ObservableCollection<DriverReplay> gara = new ObservableCollection<DriverReplay>();
        
        public Replay2()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            CaricaComboBox();

            //Sposta da qui
            Testing();
            
        }

        public async void Testing()
        {
            GetData gd = new GetData();
            var p = await gd.getPiloti();
            var c = (await gd.getCircuiti())[1].Circuit;
            var q = await gd.getResultQualifying(c.nTurn);

            foreach (var risultato in q)
            {
                var pilota = p.Find(x => x.driverId == risultato.Driver.driverId);
                var risultatoCorsa = await gd.getGraficoGara(pilota, c);
                var posizioneQualifica = Convert.ToInt32(risultato.position);

                gara.Add(new DriverReplay(pilota, risultatoCorsa, posizioneQualifica));
                //Rimuovo il pilota appena esaminato
                p.Remove(pilota);
            }

        }




        public async void CaricaComboBox()
        {
            Caricamento(true);

            GetData gd = new GetData();
            circuiti = await gd.getCircuiti();

            if (circuiti != null)
            {
                Circuito.Items.Clear();
                foreach (var circuito in circuiti)
                    if (circuito.isComplete())
                        Circuito.Items.Add(circuito.Circuit.circuitName);
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

            Caricamento(false);
        }

        private void Circuito_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = Circuito.SelectedIndex;
            if (index != -1)
            {
                circuitoSelezionato = circuiti[index].Circuit;
                Start.IsEnabled = true;
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if(circuitoSelezionato!=null)
            {
                Start.IsEnabled = false;
                Stop.IsEnabled = true;
            }
            
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = true;
            Stop.IsEnabled = false;
            Circuito.SelectedIndex = -1;
        }

        private async void Downlaod_Click(object sender, RoutedEventArgs e)
        {
            if (circuitoSelezionato != null)
            {
                ContentDialog confermaDownload = new ContentDialog
                {
                    Title = "Download dati " + circuitoSelezionato.circuitName + "?",
                    Content = "Cliccando su sì, verranno scaricati i tempi del giro di ogni pilota, così da poter essere visualizzati con più rapidità.",
                    PrimaryButtonText = "Si",
                    CloseButtonText = "No"
                };
                ContentDialogResult result = await confermaDownload.ShowAsync();
                if (result == ContentDialogResult.Primary)
                {
                    //Inizia downlaod
                    Debug.WriteLine("INIZIO DOWNLOAD DATI");
                    Update.IsActive = true;
                    await DownloadDataAndSave.DownloadDatiGP(circuitoSelezionato, "2019");
                    Debug.WriteLine("FINE DOWNLOAD DATI");
                    Update.IsActive = false;
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("alertDwn")).ShowAsync();
            }

        }

        private void Ricarica_Click(object sender, RoutedEventArgs e)
        {
            CaricaComboBox();
        }

        private void Caricamento(bool attivo)
        {
            if (attivo)
            {
                caricamento.Visibility = Visibility.Visible;
                load.IsActive = true;
            }
            else
            {
                caricamento.Visibility = Visibility.Collapsed;
                load.IsActive = false;
            }
        }

        DispatcherTimer timer;
        Stopwatch stopwatch;
        int[] giro;
        int molt = 1;
        private void Test_Click(object sender, RoutedEventArgs e)
        {
            giro = new int[gara.Count];
            molt = Convert.ToInt32(moltiplicatore.SelectedValue);
            Debug.WriteLine("Hai selezionato il moltiplicatore x" + molt);
            SetTimer();
            stopwatch = new Stopwatch();
            stopwatch.Start();
            timer.Start();
            for(int i =0; i < gara.Count; i++)
            {
                gara[i]._Giro = 1;
                giro[i] = 1;
            }
        }

        private void SetTimer()
        {
            timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(1)
            };
            timer.Tick += timer_Tick;
        }
        long tempo;
        
        private void timer_Tick(object sender, object e)
        {
            tempo = stopwatch.ElapsedMilliseconds * Convert.ToInt32(moltiplicatore.SelectedValue);
            for (int i = 0; i < gara.Count; i++)
            {
                if (gara[i].aggiorna)
                {
                    gara[i].AggiornaTempo(tempo);
                }

                if(gara[i]._Giro != giro[i])
                {
                    dataGrid.ItemsSource = new ObservableCollection<DriverReplay>(from item in gara
                                                                                  orderby item._Giro descending,  item._Posizione ascending
                                                                                  select item);
                    giro[i] = gara[i]._Giro;
                }
            }

            if (gara.ToList().FindIndex(0, gara.Count, x => x.aggiorna == true) == -1)
            {
                Debug.WriteLine("Stop al tempo!");
                timer.Stop();
            }
        }

        private void DataGrid_Sorting(object sender, Microsoft.Toolkit.Uwp.UI.Controls.DataGridColumnEventArgs e)
        {
            if (e.Column.SortDirection == null || e.Column.SortDirection == DataGridSortDirection.Ascending)
            {
                //Use the Tag property to pass the bound column name for the sorting implementation 
                if (e.Column.Tag.ToString() == "Posizione")
                {
                    //Implement ascending sort on the column "Range" using LINQ
                    dataGrid.ItemsSource = new ObservableCollection<DriverReplay>(from item in gara
                                                                                    orderby item._Posizione ascending
                                                                                    select item);
                    e.Column.SortDirection = DataGridSortDirection.Ascending;
                }
            }
        }
    }
}
