﻿using F1_Inspector.Class;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Grafici : Page
    {
        ResourceLoader resourceLoader;
        List<Driver> piloti;
        List<Race> circuiti;
        Driver a=null, b=null;
        Circuit circuit=null;
        public LineSeries delta { get; set; }

        public Grafici()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            CaricaComboBox();
        }

        public async void CaricaComboBox()
        {
            Caricamento(true);

            GetData gd = new GetData();
            var lista = await gd.getPiloti();
            circuiti = await gd.getCircuiti();

            if (lista != null && circuiti != null)
            {
                Pilota1.Items.Clear();
                Pilota2.Items.Clear();
                Circuito.Items.Clear();
                var piloti = lista.OrderBy(x => x.familyName).ToList();
                this.piloti = piloti;

                foreach (var pilota in piloti)
                {
                    Pilota1.Items.Add(pilota.fullName);
                    Pilota2.Items.Add(pilota.fullName);
                }

                foreach (var circuito in circuiti)
                    if(circuito.isComplete())
                        Circuito.Items.Add(circuito.Circuit.circuitName);
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

            Caricamento(false);
        }

        private void Confronta_Click(object sender, RoutedEventArgs e)
        {
            if (a != null && b == null)
            {
                Crea_Grafico();
            } 
            else if (a != null && b != null)
            {
                if (a.fullName != b.fullName)
                {
                    Crea_Grafico();
                }
                else
                {
                    var m = new MessageDialog(resourceLoader.GetString("ESP")).ShowAsync();
                }
            }
        }

        private void Pilota1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = Pilota1.SelectedIndex;
            if (index != -1)
            {
                a = piloti[index];
                Pilota2.IsEnabled = true;
                Confronta.IsEnabled = true;
                Clear.IsEnabled = true;
            }
        }

        private void Pilota2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = Pilota2.SelectedIndex;
            if (index != -1)
            {
                b = piloti[index];
            }
        }

        private void Circuito_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = Circuito.SelectedIndex;
            if (index != -1)
            {
                circuit = circuiti[index].Circuit;
                Pilota1.IsEnabled = true;
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Pilota1.IsEnabled = Pilota2.IsEnabled = Confronta.IsEnabled = Clear.IsEnabled = false;
            ckBoxDistacchi.IsChecked = true;
            ckBoxDistacchi.IsEnabled = false;
            Pilota1.SelectedIndex = -1;
            Pilota2.SelectedIndex = -1;
            Circuito.SelectedIndex = -1;
            Grafico.DataContext = null;
            Grafico.Visibility = Visibility.Collapsed;
            circuit = null;
            a = b = null;
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] LabelsGiri { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public async void Crea_Grafico()
        {
            Grafico.Visibility = Visibility.Collapsed;
            Grafico.DataContext = null;

            GetData gd = new GetData();
            Caricamento(true, resourceLoader.GetString("dwnData"));

            List<Lap> resultA = await gd.getGraficoGara(a, circuit);
            List<Lap> resultB = new List<Lap>();
            if (b != null)
                resultB = await gd.getGraficoGara(b, circuit);

            ChartValues<int> lstA = new ChartValues<int>();
            ChartValues<int> lstB = new ChartValues<int>();
            ChartValues<int> deltaAB = new ChartValues<int>();

            if (resultA != null)
            {
                Debug.WriteLine("Dati scaricati!");
                string[] giri = new string[circuit.TotalLaps];
                for (int i = 0; i < giri.Length; i++)
                    giri[i] = resourceLoader.GetString("GraficiGiro") + " " + (i + 1).ToString();

                SeriesCollection = new SeriesCollection();

                foreach (var giro in resultA)
                {
                    lstA.Add(TimingUtils.timeToMillisecond(giro.Timings[0].time));
                }

                var x = new LineSeries
                {
                    Title = string.Format(resourceLoader.GetString("GraficiTempo"), a.familyName),
                    Values = lstA,
                    Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 00, 183, 195)),//colore linea
                    Fill = new SolidColorBrush(Windows.UI.Colors.Transparent)//colore sotto linea
                };

                SeriesCollection.Add(x);

                if (resultB.Count() > 0)
                {
                    foreach (var giro in resultB)
                    {
                        lstB.Add(TimingUtils.timeToMillisecond(giro.Timings[0].time));
                    }

                    var y = new LineSeries
                    {
                        Title = string.Format(resourceLoader.GetString("GraficiTempo"), b.familyName),
                        Values = lstB,
                        Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 234, 0, 94)),//colore linea
                        Fill = new SolidColorBrush(Windows.UI.Colors.Transparent)//colore sotto linea
                    };
                    SeriesCollection.Add(y);

                    //Aggiungo grafici distacchi
                    int minimo = Math.Min(resultA.Count, resultB.Count);
                    int diff_prec = 0;
                    for (int i = 0; i < minimo; i++)
                    {
                        int diff = diff_prec + (TimingUtils.timeToMillisecond(resultA[i].Timings[0].time) - TimingUtils.timeToMillisecond(resultB[i].Timings[0].time));
                        diff_prec = diff;
                        deltaAB.Add(Math.Abs(diff));
                    }

                    delta = new LineSeries
                    {
                        Title = "Δt",
                        Values = deltaAB,
                        Stroke = new SolidColorBrush(Windows.UI.Colors.DarkViolet),
                        Fill = new SolidColorBrush(Windows.UI.Colors.Transparent)
                    };
                    SeriesCollection.Add(delta);
                    //Abilita check box
                    ckBoxDistacchi.IsEnabled = true;
                    ckBoxDistacchi.IsChecked = true;
                }
                else
                {
                    //Disabilita check box
                    ckBoxDistacchi.IsEnabled = false;
                }

                YFormatter = value => TimingUtils.millisecondToTime(value);
                Y.MinValue = 0;
                LabelsGiri = giri;

                Grafico.Visibility = Visibility.Visible;
                Grafico.DataContext = this;
                Caricamento(false);


            }
            else
            {
                Caricamento(false);
                var m = new MessageDialog(resourceLoader.GetString("DWD")).ShowAsync();
            }
        }

        private void CkBoxDistacchi_Checked(object sender, RoutedEventArgs e)
        {
            if(ckBoxDistacchi.IsChecked == true)
            {
                
                delta.Visibility = Visibility.Visible;
            }
            else
            {
                delta.Visibility = Visibility.Collapsed;
            }
        }

        private void Ricarica_Click(object sender, RoutedEventArgs e)
        {
            CaricaComboBox();
        }

        private async void Downlaod_Click(object sender, RoutedEventArgs e)
        {
            if(circuit!=null)
            {
                ContentDialog confermaDownload = new ContentDialog
                {
                    Title = string.Format(resourceLoader.GetString("domandadwnDatiTitolo"), circuit.circuitName),
                    Content = resourceLoader.GetString("domandadwnDati"),
                    PrimaryButtonText = resourceLoader.GetString("si"),
                    CloseButtonText = resourceLoader.GetString("no")
                };
                ContentDialogResult result = await confermaDownload.ShowAsync();
                if (result == ContentDialogResult.Primary)
                {
                    //Inizia downlaod
                    Debug.WriteLine("INIZIO DOWNLOAD DATI");
                    Update.IsActive = true;
                    await DownloadDataAndSave.DownloadDatiGP(circuit, GetData.Anno);
                    Debug.WriteLine("FINE DOWNLOAD DATI");
                    Update.IsActive = false;
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("alertDwn")).ShowAsync();
            }
            
        }

        private void Caricamento(bool attivo,string information="")
        {

            if (attivo)
            {
                caricamento.Visibility = Visibility.Visible;
                load.IsActive = true;
                if (information != "")
                    mainInfo.Text = information;
                else
                    mainInfo.Text = resourceLoader.GetString("caricamento");
            }
            else
            {
                caricamento.Visibility = Visibility.Collapsed;
                load.IsActive = false;
            }
        }
    }
}
