﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Scuderie
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Scuderie : Page
    {
        ResourceLoader resourceLoader;
        public Scuderie()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            MostraScuderie();
        }

        private void ScuderieView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var scuderia = ScuderieView.SelectedItem;
            Frame.Navigate(typeof(ScuderieDetails), scuderia);
        }

        public async void MostraScuderie()
        {
            load.IsActive = true;

            GetData gd = new GetData();
            var lista = await gd.getCostruttori();

            if(lista != null)
            {
                var macchine = (from x in lista
                                orderby x.name
                                select x).ToList();

                for (int i = 0; i < macchine.Count; i++)
                {
                    ScuderieView.Items.Add(macchine[i]);
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

            load.IsActive = false;
        }
    }
}
