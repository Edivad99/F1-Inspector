﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Scuderie
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class ScuderieDetails : Page
    {
        ResourceLoader resourceLoader;
        public ScuderieDetails()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }

        Constructor costruttore;
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            load.IsActive = true;

            if (e.Parameter is Constructor)
                costruttore = e.Parameter as Constructor;
            else
            {
                var temp = e.Parameter as ConstructorStanding;
                costruttore = temp.Constructor;
            }

            //Statistiche pilota
            ScuderiaName.Text = costruttore.name;
            ScuderiaName.Foreground = new SolidColorBrush(Windows.UI.Colors.White);

            var Statistiche = await costruttore.Statistiche();
            int vittorieAnnoCorrente = await costruttore.VittorieAnnoCorrente();

            if (Statistiche != null && vittorieAnnoCorrente != -1)
            {
                Team.Text += Statistiche["Full Team Name"];
                Base.Text += Statistiche["Base"];
                TeamPrincipal.Text += Statistiche["Team Chief"];
                TechnicalChief.Text += Statistiche["Technical Chief"];
                Chassis.Text += Statistiche["Chassis"];
                PowerUnit.Text += Statistiche["Power Unit"];
                VittorieStagioneCorrente.Text += vittorieAnnoCorrente;
                FirstTeamEntry.Text += Statistiche["First Team Entry"];

                if (Statistiche["World Championships"] == "N/A")
                    TitoliCostruttori.Text += "0";
                else
                    TitoliCostruttori.Text += Statistiche["World Championships"];
                HighestRaceFinish.Text += Statistiche["Highest Race Finish"];
                if (Statistiche["Pole Positions"] == "N/A")
                    PolePositions.Text += "0";
                else
                    PolePositions.Text += Statistiche["Pole Positions"];
                if (Statistiche["Fastest Laps"] == "N/A")
                    FastestLaps.Text += "0";
                else
                    FastestLaps.Text += Statistiche["Fastest Laps"];

                //Macchina
                var immagineMacchina = new BitmapImage(new Uri(costruttore.ImageCar));
                foto.Source = immagineMacchina;
                BaseFoto.Fill = costruttore.colorConstructor;
                load.IsActive = false;
            }
            else
            {
                load.IsActive = false;
                var m = new MessageDialog(resourceLoader.GetString("EIC"));
                m.Commands.Add(new UICommand("OK") { Id = 0 });
                m.DefaultCommandIndex = 0;
                var result = await m.ShowAsync();
                if ((int)result.Id == 0)
                {
                    if (this.Frame.CanGoBack)
                        this.Frame.GoBack();
                }
            }
        }
    }
}
