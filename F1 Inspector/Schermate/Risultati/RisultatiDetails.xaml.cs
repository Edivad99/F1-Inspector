﻿using F1_Inspector.Class;
using F1_Inspector.Schermate.Piloti;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Risultati
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class RisultatiDetails : Page
    {
        ResourceLoader resourceLoader;
        public RisultatiDetails()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            load.IsActive = true;
            int round = int.Parse(e.Parameter as string);
            GetData gd = new GetData();

            var corsa = await gd.getResultRace(round);
            var qualifica = await gd.getResultQualifying(round);
            if(corsa != null && qualifica !=null)
            {
                foreach (var x in corsa.Races[0].Results)
                {
                    RaceView.Items.Add(x);
                }

                foreach(var x in qualifica)
                {
                    QualifyingView.Items.Add(x);
                }

                Titolo.Text = corsa.Races[0].raceName;
                load.IsActive = false;
            }
            else
            {
                load.IsActive = false;
                var m = new MessageDialog(resourceLoader.GetString("EIC"));
                m.Commands.Add(new UICommand("OK") { Id = 0 });
                m.DefaultCommandIndex = 0;
                var result = await m.ShowAsync();
                if ((int)result.Id == 0)
                {
                    if (this.Frame.CanGoBack)
                        this.Frame.GoBack();
                }
            }
            
        }

        private void RaceView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = RaceView.SelectedItem;
            Frame.Navigate(typeof(RisultatiStagionali), x);
        }
    }
}
