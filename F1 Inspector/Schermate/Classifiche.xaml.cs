﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Classifiche : Page
    {
        ResourceLoader resourceLoader;
        public Classifiche()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            CaricaClassifiche();
        }

        public async void CaricaClassifiche()
        {
            load.IsActive = true;
            int c1 = await ClassificaPiloti();
            int c2 = await ClassificaCostruttori();
            load.IsActive = false;
            if (c1 == 1 || c2 == 1)
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }
        }
        
        private async Task<int> ClassificaPiloti()
        {
            var data= new GetData();
            var classificapiloti = await data.getClassificaPiloti();
            if(classificapiloti != null)
            {
                for (int i = 0; i < classificapiloti.Count; i++)
                {
                    PilotiView.Items.Add(classificapiloti[i]);
                }
                return 0;
            }
            else
            {
                return 1;
            }
            
        }

        private async Task<int> ClassificaCostruttori()
        {
            var data = new GetData();
            var classificacostruttori = await data.getClassificaCostruttori();
            if(classificacostruttori != null)
            { 
                for (int i = 0; i < classificacostruttori.Count; i++)
                {
                    ScuderieView.Items.Add(classificacostruttori[i]);
                }
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private void PilotiView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pilota = PilotiView.SelectedItem;
            Frame.Navigate(typeof(PilotiDetails), pilota);
        }

        private void ScuderieView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var scuderia = ScuderieView.SelectedItem;
            Frame.Navigate(typeof(Scuderie.ScuderieDetails), scuderia);
        }
    }
}
