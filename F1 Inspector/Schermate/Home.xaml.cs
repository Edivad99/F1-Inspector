﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        DispatcherTimer timer;
        DateTime endTime;
        ResourceLoader resourceLoader;
        ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
        ObservableCollection<Articolo> articoliHome;

        public Home()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            Initialize();
        }
        private void Initialize()
        {
            setTimer();
            setRanking();
            setHomeNewsAsync();
        }

        private async void setTimer()
        {
            var x = new GetData();
            var gara = await x.getNextRace();

            if (gara != null)
            {
                GranPrix.Text = gara.raceName;

                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 1);
                timer.Tick += timer_Tick;

                string data = gara.date;
                string ora = gara.getLocalTime().Substring(0, gara.getLocalTime().Length - 1).Split(' ')[1];
                string[] dataVett = data.Split('-');
                string[] oraVett = ora.Split(':');

                endTime = new DateTime(int.Parse(dataVett[0]), int.Parse(dataVett[1]), int.Parse(dataVett[2]), int.Parse(oraVett[0]), int.Parse(oraVett[1]), int.Parse(oraVett[2]));

                TimeSpan ts = endTime.Subtract(DateTime.Now);
                if (ts.Days < 0 || ts.Hours < 0 || ts.Minutes < 0 || ts.Seconds < 0)
                {
                    DaysCount.Text = HourCount.Text = MinuteCount.Text = SecondCount.Text = "0";
                }
                else
                {
                    DaysCount.Text = ts.Days.ToString();
                    HourCount.Text = ts.Hours.ToString();
                    MinuteCount.Text = ts.Minutes.ToString();
                    SecondCount.Text = ts.Seconds.ToString();
                    timer.Start();
                }

            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

        }

        private void timer_Tick(object sender, object e)
        {
            TimeSpan ts = endTime.Subtract(DateTime.Now);
            DaysCount.Text = ts.Days.ToString();
            HourCount.Text = ts.Hours.ToString();
            MinuteCount.Text = ts.Minutes.ToString();
            SecondCount.Text = ts.Seconds.ToString();
        }

        private async void setRanking()
        {
            GetData gd = new GetData();

            var classificaPiloti = await gd.getClassificaPiloti();
            var classificaCostruttori = await gd.getClassificaCostruttori();

            if(classificaPiloti != null)
            {
                posto1.Text = classificaPiloti[0].Driver.fullName;
                posto2.Text = classificaPiloti[1].Driver.fullName;
                posto3.Text = classificaPiloti[2].Driver.fullName;

                punti1.Text = classificaPiloti[0].points;
                punti2.Text = classificaPiloti[1].points;
                punti3.Text = classificaPiloti[2].points;

                if(classificaCostruttori != null)
                {
                    scuderia1.Text = classificaCostruttori[0].Constructor.name;
                    puntiScuderia1.Text = classificaCostruttori[0].points;
                    var immagineMacchina = new BitmapImage(new Uri(classificaCostruttori[0].Constructor.ImageCar));
                    fotoMacchina.Source = immagineMacchina;
                }
                else
                {
                    var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }
        }

        private async void setHomeNewsAsync()
        {
            try
            {
                if (localSettings.Values["giornaleHome"] != null)
                {
                    GestoreArticoli gs = new GestoreArticoli();
                    articoliHome = await gs.GetArticoliByName(localSettings.Values["giornaleHome"].ToString());
                    if (articoliHome != null)
                    {
                        foreach (var articolo in articoliHome)
                        {
                            lstGiornali.Items.Add(articolo.Titolo);
                        }
                    }
                }
            }
            catch
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }
        }

        private void LstGiornali_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var i = lstGiornali.SelectedIndex;

            var parametro = new ArticoliParameter(articoliHome, i);
            Frame.Navigate(typeof(NewsArticle), parametro);
        }
    }
}
