﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Impostazioni : Page
    {
        List<string> giornaliAttivi = new List<string>();
        ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        public Impostazioni()
        {
            this.InitializeComponent();

            if (localSettings.Values["giornali"] != null)
            {
                var supp = localSettings.Values["giornali"].ToString().Split('#');
                foreach (var x in supp)
                {
                    if (x != "" && x.Length > 1)
                        giornaliAttivi.Add(x);
                    switch (x)
                    {
                        case "F1GrandPrix": F1GrandPrix.IsChecked = true; break;
                        case "FormulaPassion": FormulaPassion.IsChecked = true; break;
                        case "Motorsport.com": Motorsportcom.IsChecked = true; break;
                    }
                }
            }
            else
                localSettings.Values["giornali"] = "";

            //Add giornali cmbBox
            List<string> giornali = new List<string>();
            giornali.Add("F1GrandPrix");
            giornali.Add("FormulaPassion");
            giornali.Add("Motorsport.com");

            foreach (var x in giornali)
                cmbGiornale.Items.Add(x);

            if(localSettings.Values["giornaleHome"] != null)
            {
                int i = cmbGiornale.Items.IndexOf(localSettings.Values["giornaleHome"]);
                cmbGiornale.SelectedIndex = i;
            }
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            DownloadDataAndSave.DeleteAll();
        }
        private void Giornali_Changed(object sender, RoutedEventArgs e)
        {
            if (F1GrandPrix.IsChecked == true)
            {
                if (!giornaliAttivi.Contains("F1GrandPrix"))
                    giornaliAttivi.Add("F1GrandPrix");
            }
            else
                giornaliAttivi.Remove("F1GrandPrix");

            if (FormulaPassion.IsChecked == true)
            {
                if (!giornaliAttivi.Contains("FormulaPassion"))
                    giornaliAttivi.Add("FormulaPassion");
            }
            else
                giornaliAttivi.Remove("FormulaPassion");

            if (Motorsportcom.IsChecked == true)
            {
                if (!giornaliAttivi.Contains("Motorsport.com"))
                    giornaliAttivi.Add("Motorsport.com");
            }
            else
                giornaliAttivi.Remove("Motorsport.com");

            Debug.WriteLine("*++++++++++++++++++++++*");
            string s = "";
            foreach (var x in giornaliAttivi)
            {
                Debug.WriteLine(x);
                s += x + "#";
            }
            if (giornaliAttivi.Count > 0)
                s = s.Substring(0, s.Length - 1);
            //salvo i giornali
            localSettings.Values["giornali"] = s;
            Debug.WriteLine("*++++++++++++++++++++++*");
        }

        private void CmbGiornale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = (string)cmbGiornale.SelectedItem;
            if (x != null)
                localSettings.Values["giornaleHome"] = x;
        }
    }
}
