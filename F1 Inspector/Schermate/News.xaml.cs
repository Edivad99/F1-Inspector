﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class News : Page
    {
        ApplicationDataContainer localSettings;
        ResourceLoader resourceLoader;
        public News()
        {

            this.InitializeComponent();
            var currentView = SystemNavigationManager.GetForCurrentView();
            currentView.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            localSettings = ApplicationData.Current.LocalSettings;
            resourceLoader = ResourceLoader.GetForCurrentView();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                getArticoliAsync();
                //pubblicita.Visibility = Visibility.Visible;
                //pubblicita.IsAutoRefreshEnabled = true;
                //pubblicita.ApplicationId = "9nthjhlp8f0b";
                //pubblicita.AdUnitId = "1100026076";
            }
        }

        ObservableCollection<Articolo> lista = new ObservableCollection<Articolo>();

        public async void getArticoliAsync()
        {
            load.IsActive = true;
            try
            {
                if (localSettings.Values["giornali"] != null && localSettings.Values["giornali"].ToString() != "")
                {
                    var supp = localSettings.Values["giornali"].ToString().Split('#');
                    GestoreArticoli gs = new GestoreArticoli();
                    foreach (var x in supp)
                    {
                        var z = await gs.GetArticoliByName(x);
                        if (z != null)
                            foreach (var i in z)
                                lista.Add(i);
                    }
                }
                else
                {
                       var m = new MessageDialog(resourceLoader.GetString("selectGiornali")).ShowAsync();
                }
            }
            catch
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }
            
            load.IsActive = false;
        }

        private void articoli_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var i = GVArticoli.SelectedIndex;

            var parametro = new ArticoliParameter(lista, i);
            Frame.Navigate(typeof(NewsArticle), parametro);
        }

        //private void pubblicita_ErrorOccurred(object sender, Microsoft.Advertising.WinRT.UI.AdErrorEventArgs e)
        //{
        //    Debug.WriteLine("Errore AD: " + e.ErrorMessage);
        //}
    }
}