﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class PilotiDetails : Page
    {
        ResourceLoader resourceLoader;
        public PilotiDetails()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }

        Driver pilot;
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            load.IsActive = true;
            bool errore = false;

            if (e.Parameter is Driver)
                pilot = e.Parameter as Driver;
            else
            {
                var temp = e.Parameter as DriverStanding;
                pilot = temp.Driver;
            }

            //Statistiche pilota
            PilotaName.Text = pilot.fullName;
            Number.Text += pilot.permanentNumber;
            var Statistiche = await pilot.Statistiche();

            if(Statistiche!=null)
            {
                Team.Text += Statistiche["Team"];
                Country.Text += Statistiche["Country"];
                if (Statistiche["Podiums"] == "N/A")
                {
                    Podiums.Text += "0";
                }
                else
                {
                    Podiums.Text += Statistiche["Podiums"];
                }
                Points.Text += Statistiche["Points"];
                GrandsPrixEntered.Text += Statistiche["Grands Prix entered"];
                if(Statistiche["World Championships"] == "N/A")
                {
                    WorldChampionships.Text += "0";
                }
                else
                {
                    WorldChampionships.Text += Statistiche["World Championships"];
                }
                HighestRaceFinish.Text += Statistiche["Highest race finish"];
                HighestGridPosition.Text += Statistiche["Highest grid position"];
                DateBirth.Text += Statistiche["Date of birth"];
                PlaceBirth.Text += Statistiche["Place of birth"];

                try
                {
                    //Immagine del pilota
                    foto.Source = new BitmapImage(new Uri(pilot.Image2));

                    //Flag
                    var flag = new BitmapImage(new Uri(Statistiche["Flag"], UriKind.Absolute));
                    Flag.ImageSource = flag;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Errore download immagini: " + ex.Message);
                    errore = true;
                }
            }
            else
            {
                errore = true;
            }

            if(errore)
            {
                load.IsActive = false;
                var m = new MessageDialog(resourceLoader.GetString("EIC"));
                m.Commands.Add(new UICommand("OK") { Id = 0 });
                m.DefaultCommandIndex = 0;
                var result = await m.ShowAsync();
                if ((int)result.Id == 0)
                {
                    if (this.Frame.CanGoBack)
                        this.Frame.GoBack();
                }
            }
            else
            {
                load.IsActive = false;
            }
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Piloti.RisultatiStagionali), pilot);
        }
    }
}
