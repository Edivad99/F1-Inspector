﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Piloti
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Piloti : Page
    {
        ResourceLoader resourceLoader;
        public Piloti()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
            MostraPiloti();
        }

        public async void MostraPiloti()
        {
            load.IsActive = true;

            GetData gd = new GetData();
            var lista = await gd.getPiloti();
            if (lista != null)
            {
                var piloti = (from x in lista
                              orderby x.constructorName, x.fullName
                              select x).ToList();

                foreach(var pilota in piloti)
                {
                    PilotiView.Items.Add(pilota);
                    //Debug.WriteLine(pilota.fullName + " => " + await pilota.getWikipediaDescription());
                }
            }
            else
            {
                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
            }

            load.IsActive = false;
        }

        private void PilotiView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pilota = PilotiView.SelectedItem;
            Frame.Navigate(typeof(PilotiDetails), pilota);
        }
    }
}
