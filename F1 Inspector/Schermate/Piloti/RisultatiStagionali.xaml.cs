﻿using F1_Inspector.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace F1_Inspector.Schermate.Piloti
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class RisultatiStagionali : Page
    {
        ResourceLoader resourceLoader;

        public RisultatiStagionali()
        {
            this.InitializeComponent();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            load.IsActive = true;
            List<Race> stagione;
            if (e.Parameter is Driver)
            {
                var Risultati = e.Parameter as Driver;
                Pilota.Text = Risultati.fullName;
                GetData gd = new GetData();
                stagione = await gd.getResultRacesDriver(Risultati.driverId);

            }
            else
            {
                var Risultati = e.Parameter as Result;
                Pilota.Text = Risultati.Driver.fullName;
                GetData gd = new GetData();
                stagione = await gd.getResultRacesDriver(Risultati.Driver.driverId);
            }
            
            if(stagione!=null)
            {
                foreach (var a in stagione)
                {
                    RaceView.Items.Add(a);
                }
                load.IsActive = false;
            }
            else
            {
                load.IsActive = false;
                var m = new MessageDialog(resourceLoader.GetString("InizioVuotoStagione"));
                m.Commands.Add(new UICommand("OK") { Id = 0 });
                m.DefaultCommandIndex = 0;
                var result = await m.ShowAsync();
                if((int)result.Id == 0)
                {
                    if (this.Frame.CanGoBack)
                        this.Frame.GoBack();
                }
            }
        }

        private void RaceView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var gara = RaceView.SelectedItem as Race;
            Frame.Navigate(typeof(Risultati.RisultatiDetails), gara.round);
        }
    }
}
