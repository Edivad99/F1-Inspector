﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace F1_Inspector.Class
{
    public class Time
    {
        public string millis { get; set; }
        public string time { get; set; }
    }

    public class Time2
    {
        public string time { get; set; }
    }

    public class AverageSpeed
    {
        public string units { get; set; }
        public string speed { get; set; }
    }

    public class FastestLap
    {
        public string rank { get; set; }
        public string lap { get; set; }
        public Time2 Time { get; set; }
        public AverageSpeed AverageSpeed { get; set; }
    }

    public class Result
    {
        public string number { get; set; }
        public string position { get; set; }
        public string positionText { get; set; }// R -> Ritirato
        public string points { get; set; }
        public Driver Driver { get; set; }
        public Constructor Constructor { get; set; }
        public string grid { get; set; }
        public string laps { get; set; }
        public string status { get; set; }
        public Time Time { get; set; }
        public string TempoGriglia
        {
            get
            {
                if(status == "Finished")
                {
                    return Time.time;
                }
                else
                {
                    return status;
                }
            }
        }

        public FastestLap FastestLap { get; set; }

        public SolidColorBrush colorePodio
        {
            get
            {
                if (position == "1")
                    return new SolidColorBrush(Windows.UI.Colors.Gold);
                else if (position == "2")
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255,169, 169, 169));
                else if (position == "3")
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 80, 50, 20));//bronze
                else if(status=="Finished" || status[0]=='+')
                {
                    var AppTheme = Application.Current.RequestedTheme;
                    if (AppTheme ==  ApplicationTheme.Dark)//Dark
                    {
                        return new SolidColorBrush(Windows.UI.Colors.White);
                    }
                    else//Light
                    {
                        return new SolidColorBrush(Windows.UI.Colors.Black);
                    }
                }
                else
                {
                    return new SolidColorBrush(Windows.UI.Colors.Red);
                }
            }
        }

        public Visibility puntoBonus
        {
            get
            {
                if (extraPointForFastestlap())
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
        }

        public bool extraPointForFastestlap()
        {
            return FastestLap.rank == "1";
        }
    }

    public class ResulRaceParent
    {
        public MRData MRData { get; set; }
    }
}
