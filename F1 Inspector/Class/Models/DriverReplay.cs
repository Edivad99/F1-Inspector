﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1_Inspector.Class
{
    class DriverReplay : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<Lap> gara;
        private Driver driver;
        public int posizioneQualifica;

        public bool aggiorna;

        //Variabili di Backup
        private string GAP;
        private int Progresso, Giro, Posizione;
        private double QuestoGiro, UltimoGiro, MigliorGiro;


        private int lap;
        private List<int> tempiPrecendenti = new List<int>();
        private int tempiPrecedentiSum = 0;
        private int indice = 0;


        public string _Name
        {
            get
            {
                return driver.familyName;
            }
        }
        public int _Progresso
        {
            get
            {
                return Progresso;
            }
            set
            {
                if (value != Progresso)
                {
                    Progresso = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_Progresso)));
                }
                    
            }
        }
        public int _Giro
        {
            get
            {
                return Giro;
            }
            set
            {
                if (value != Giro)
                {
                    Giro = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_Giro)));
                }
                    
            }
        }
        public int _Posizione
        {
            get
            {
                return Posizione;
            }
            set
            {
                if (value != Posizione)
                {
                    Posizione = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_Posizione)));
                }
                    
            }
        }
        public string _GAP
        {
            get
            {
                return GAP;
            }
            set
            {
                if (value != GAP)
                    GAP = value;
            }
        }
        public string _QuestoGiro
        {
            get
            {
                return TimingUtils.millisecondToTime(QuestoGiro);
            }
            set
            {
                if(QuestoGiro != TimingUtils.timeToMillisecond(value))
                {
                    QuestoGiro = TimingUtils.timeToMillisecond(value);
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_QuestoGiro)));
                }
                
            }

        }
        public string _UltimoGiro
        {
            get
            {
                return TimingUtils.millisecondToTime(UltimoGiro);
            }
            set
            {
                if (UltimoGiro != TimingUtils.timeToMillisecond(value))
                {
                    UltimoGiro = TimingUtils.timeToMillisecond(value);
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_UltimoGiro)));
                }

            }

        }
        public string _MigliorGiro
        {
            get
            {
                return TimingUtils.millisecondToTime(MigliorGiro) + string.Format(" ({0})",_Giro);
            }
            set
            {
                if (MigliorGiro != TimingUtils.timeToMillisecond(value))
                {
                    MigliorGiro = TimingUtils.timeToMillisecond(value);
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(_MigliorGiro)));
                }

            }
        }

        public DriverReplay(Driver driver, List<Lap>gara, int posizioneQualifica)
        {
            this.driver = driver;
            this.gara = gara;
            this.posizioneQualifica = posizioneQualifica;
            aggiorna = true;

            Progresso = 0;
            Giro = 0;
            Posizione = posizioneQualifica;
            GAP = "±0:00.000";
            QuestoGiro = UltimoGiro = MigliorGiro = 0;

            lap = gara[indice].Timings[0].millisecondTime;
            
        }

        public void AggiornaTempo(double millisecond)
        {
            if (millisecond - tempiPrecedentiSum > lap && indice < gara.Count) //cambio giro
            {
                tempiPrecendenti.Add(lap);
                tempiPrecedentiSum = tempiPrecendenti.Sum();
                _UltimoGiro = gara[indice].Timings[0].time;

                if (UltimoGiro < MigliorGiro || _Giro == 1)
                    _MigliorGiro = _UltimoGiro;

                _Posizione = Convert.ToInt32(gara[indice].Timings[0].position);

                if (indice < gara.Count -1)//Finche ho ancora giri da fare
                {
                    _Giro++;
                    indice++;
                    lap = gara[indice].Timings[0].millisecondTime;//giro che farò la prossima volta
                }
                else
                {
                    _Progresso = 100;
                    _QuestoGiro = gara[indice].Timings[0].time;
                    aggiorna = false;
                }
            }

            if(aggiorna)
            {
                _QuestoGiro = TimingUtils.millisecondToTime((millisecond - tempiPrecedentiSum));
                int prog = Convert.ToInt32(((millisecond - tempiPrecedentiSum) / lap) * 100);
                _Progresso = prog;
            }
        }
    }
}
