﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1_Inspector.Class
{
    public class DriverStanding
    {
        public string position { get; set; }
        public string positionText { get; set; }
        public string points { get; set; }
        public string wins { get; set; }
        public Driver Driver { get; set; }
        public List<Constructor> Constructors { get; set; }
    }

    public class ClassificaParent
    {
        public MRData MRData { get; set; }
    }

    public class QualifyingParent
    {
        public MRData MRData { get; set; }
    }
}
