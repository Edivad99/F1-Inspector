﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace F1_Inspector.Class
{
    public class Driver
    {
        public string driverId { get; set; }
        public string permanentNumber { get; set; }
        public string code { get; set; }
        public string url { get; set; }
        public string givenName { get; set; }
        public string familyName { get; set; }
        //public string dateOfBirth { get; set; }
        //public string nationality { get; set; }
        public string fullName { get { return givenName + " " + familyName; } }
        public string URLStats { get { return "https://www.formula1.com/en/drivers/" + uniformString(givenName.ToLower()) + "-" + uniformString(familyName.ToLower()) + ".html"; } }
        public string Image { get { return string.Format("ms-appx:///Assets/img/Drivers/body/{0}.jpg", driverId); } }
        public string Image2 { get { return string.Format("ms-appx:///Assets/img/Drivers/head/{0}.png", driverId); } }
        public string constructorName
        {
            get
            {
                switch (driverId)
                {
                    case "raikkonen":
                    case "giovinazzi":
                        return "Alfa Romeo";

                    case "vettel":
                    case "leclerc":
                        return "Ferrari";

                    case "grosjean":
                    case "kevin_magnussen":
                        return "Haas";

                    case "sainz":
                    case "norris":
                        return "McLaren";

                    case "hamilton":
                    case "bottas":
                        return "Mercedes";

                    case "perez":
                    case "stroll":
                        return "Racing Point";

                    case "max_verstappen":
                    case "gasly":
                        return "Red Bull";

                    case "hulkenberg":
                    case "ricciardo":
                        return "Renault";

                    case "kvyat":
                    case "albon":
                        return "Toro Rosso";
                        
                    case "russell":
                    case "kubica":
                        return "Williams";
                    default: return "";
                }
            }
        }
        public SolidColorBrush colorConstructor { get { return Constructor.getColorConstructor(constructorName); } }
        
        public async Task<Dictionary<string, string>> Statistiche()
        {
            try
            {
                HttpClient client = new HttpClient();
                var url = new Uri(URLStats);
                var response = await client.GetStringAsync(url);

                var doc = new HtmlDocument();
                doc.LoadHtml(response);

                var chiaveRAW = doc.DocumentNode.SelectNodes("//section[@class='stats']//div//table//tbody//tr//th//span");
                var valoreRAW = doc.DocumentNode.SelectNodes("//section[@class='stats']//div//table//tbody//tr//td");
                string flagURL;
                if (driverId != "albon")
                    flagURL = "https://www.formula1.com" + doc.DocumentNode.SelectSingleNode("//span[@class='icn-flag']//img").Attributes["src"].Value;
                else
                    flagURL = "https://cdn.countryflags.com/thumbs/thailand/flag-400.png";
                var helmetURL = "https://www.formula1.com" + doc.DocumentNode.SelectSingleNode("//div[@class='brand-logo']//img").Attributes["src"].Value;
                Dictionary<string, string> stats = new Dictionary<string, string>();
                for (int i = 0; i < chiaveRAW.Count; i++)
                {
                    stats.Add(chiaveRAW[i].InnerText.Trim(), valoreRAW[i].InnerText.Trim());
                }
                stats.Add("Flag", flagURL);
                stats.Add("Helmet", helmetURL);
                return stats;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Errore download statistiche Pilota: " + ex.Message);
                return null;
            }
            
        }

        private string uniformString(string text)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            byte[] tempBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(text);
            return Encoding.UTF8.GetString(tempBytes);
        }


        //In sviluppo
        public async Task<string> getWikipediaDescription()
        {
            try
            {
                HttpClient client = new HttpClient();
                var url = new Uri(this.url);
                var response = await client.GetStringAsync(url);

                var doc = new HtmlDocument();
                doc.LoadHtml(response);

                var testo = System.Net.WebUtility.HtmlDecode(doc.DocumentNode.SelectSingleNode("//*[@id='mw-content-text']/div/p[2]").InnerText).Trim();
                return testo;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errore wikipedia Pilota: " + ex.Message);
                return "";
            }
        }
    }

    public class DriverTable
    {
        public string season { get; set; }
        public List<Driver> Drivers { get; set; }
    }

    public class DriverParent
    {
        public MRData MRData { get; set; }
    }
}
