﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Media;

namespace F1_Inspector.Class
{
    public class Location
    {
        public string lat { get; set; }
        public string @long { get; set; }
        public string locality { get; set; }
        public string country { get; set; }
        public string flag
        {
            get
            { 
                switch (country)
                {
                    case "Australia": return "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Flag_of_Australia.svg/280px-Flag_of_Australia.svg.png";
                    case "Bahrain": return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Bahrain.svg/280px-Flag_of_Bahrain.svg.png";
                    case "China": return "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/280px-Flag_of_the_People%27s_Republic_of_China.svg.png";
                    case "Azerbaijan": return "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/280px-Flag_of_Azerbaijan.svg.png";
                    case "Spain": return "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/280px-Flag_of_Spain.svg.png";
                    case "Monaco": return "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Flag_of_Monaco.svg/280px-Flag_of_Monaco.svg.png";
                    case "Canada": return "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Flag_of_Canada.svg/280px-Flag_of_Canada.svg.png";
                    case "France": return "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/280px-Flag_of_France.svg.png";
                    case "Austria": return "https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/280px-Flag_of_Austria.svg.png";
                    case "UK": return "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/280px-Flag_of_the_United_Kingdom.svg.png";
                    case "Germany": return "https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/280px-Flag_of_Germany.svg.png";
                    case "Hungary": return "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Flag_of_Hungary.svg/280px-Flag_of_Hungary.svg.png";
                    case "Belgium": return "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_Belgium.svg/280px-Flag_of_Belgium.svg.png";
                    case "Italy": return "https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/280px-Flag_of_Italy.svg.png";
                    case "Singapore": return "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Singapore.svg/280px-Flag_of_Singapore.svg.png";
                    case "Russia": return "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/280px-Flag_of_Russia.svg.png";
                    case "Japan": return "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/280px-Flag_of_Japan.svg.png";
                    case "USA": return "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Flag_of_the_United_States_%28Pantone%29.svg/280px-Flag_of_the_United_States_%28Pantone%29.svg.png";
                    case "Mexico": return "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/280px-Flag_of_Mexico.svg.png";
                    case "Brazil": return "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Brazil.svg/280px-Flag_of_Brazil.svg.png";
                    case "UAE": return "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Flag_of_the_United_Arab_Emirates.svg/280px-Flag_of_the_United_Arab_Emirates.svg.png";
                    default : return null;
                }
            }
        }

        public static implicit operator Geopoint(Location v)
        {
            throw new NotImplementedException();
        }
    }

    public class Circuit
    {
        public string circuitId { get; set; }
        public string url { get; set; }
        public string circuitName { get; set; }
        public Location Location { get; set; }
        public string URLStats
        {
            get
            {
                switch(Location.country)
                {
                    case "UAE": return "https://www.formula1.com/en/racing/2019/United_Arab_Emirates.html";
                    case "UK":  return "https://www.formula1.com/en/racing/2019/Great_Britain.html";
                    case "USA": return "https://www.formula1.com/en/racing/2019/United_States.html";
                    default:    return "https://www.formula1.com/en/racing/2019/" + Location.country + ".html";
                }
            }
        }
        public int TotalLaps
        {
            get
            {
                switch (circuitId)
                {
                    case "albert_park": return 58;
                    case "americas": return 56;
                    case "bahrain": return 57;
                    case "BAK": return 51;
                    case "catalunya": return 66;
                    case "hockenheimring": return 67;
                    case "hungaroring": return 70;
                    case "interlagos": return 71;
                    case "marina_bay": return 61;
                    case "monaco": return 78;
                    case "monza": return 53;
                    case "red_bull_ring": return 71;
                    case "ricard": return 53;
                    case "rodriguez": return 71;
                    case "shanghai": return 56;
                    case "silverstone": return 52;
                    case "sochi": return 53;
                    case "spa": return 44;
                    case "suzuka": return 53;
                    case "villeneuve": return 70;
                    case "yas_marina": return 55;
                    default: return -1;
                }
            }
        }

        public int nTurn
        {
            get
            {
                switch (circuitId)
                {
                    case "albert_park": return 1;
                    case "bahrain": return 2;
                    case "shanghai": return 3;
                    case "BAK": return 4;
                    case "catalunya": return 5;
                    case "monaco": return 6;
                    case "villeneuve": return 7;
                    case "ricard": return 8;
                    case "red_bull_ring": return 9;
                    case "silverstone": return 10;
                    case "hockenheimring": return 11;
                    case "hungaroring": return 12;
                    case "spa": return 13;
                    case "monza": return 14;
                    case "marina_bay": return 15;
                    case "sochi": return 16;
                    case "suzuka": return 17;
                    case "rodriguez": return 18;
                    case "americas": return 19;
                    case "interlagos": return 20;
                    case "yas_marina": return 21;
                    default: return -1;
                }
            }
        }

        public async Task<Dictionary<string, string>> Statistiche()
        {
            try
            {
                HttpClient client = new HttpClient();
                var url = new Uri(URLStats);
                var response = await client.GetStringAsync(url);

                var doc = new HtmlDocument();
                doc.LoadHtml(response);

                var valoreRAW = doc.DocumentNode.SelectNodes("//div[@class='container']//div[@class='f1-race-hub--map']//fieldset//div//div[@class='col-xl-5']//div//div//div//p");
                //TrackURL
                Dictionary<string, string> track = new Dictionary<string, string>
                {
                    { "Australia", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Australia_Circuit.png" },
                    { "Bahrain", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Bahrain_Circuit.png" },
                    { "China", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/China_Circuit.png" },
                    { "Azerbaijan", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Baku_Circuit.png" },
                    { "Spain", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Spain_Circuit.png" },
                    { "Monaco", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Monoco_Circuit.png" },
                    { "Canada", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Canada_Circuit.png" },
                    { "France", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/France_Circuit.png" },
                    { "Austria", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Austria_Circuit.png" },
                    { "UK", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Great_Britain_Circuit.png" },
                    { "Germany", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Germany_Circuit.png" },
                    { "Hungary", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Hungary_Circuit.png" },
                    { "Belgium", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Belgium_Circuit.png" },
                    { "Italy", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Italy_Circuit.png" },
                    { "Singapore", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Singapore_Circuit.png" },
                    { "Russia", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Russia_Circuit.png" },
                    { "Japan", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Japan_Circuit.png" },
                    { "Mexico", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Mexico_Circuit.png" },
                    { "USA", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/USA_Circuit.png" },
                    { "Brazil", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Brazil_Circuit.png" },
                    { "UAE", "https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Circuit%20maps%2016x9/Abu_Dhabi_Circuit.png" }
                };

                //Aggiusto i valori sulle lunghezze 2568,25km

                string CircuitLength = valoreRAW[5].InnerText.Trim().Split('k')[0].ToString().Replace('.',',') + " Km";
                string RaceDistance = valoreRAW[7].InnerText.Trim().Split('k')[0].ToString().Replace('.', ',') + " Km";

                Dictionary<string, string> stats = new Dictionary<string, string>
                {
                    { "FIRST GRAND PRIX", valoreRAW[1].InnerText.Trim() },
                    { "NUMBER OF LAPS", valoreRAW[3].InnerText.Trim() },
                    { "CIRCUIT LENGTH", CircuitLength },
                    { "RACE DISTANCE", RaceDistance },
                    { "LAP RECORD", valoreRAW[9].InnerText.Trim() },
                    { "Track image", track[Location.country] }
                };
                return stats;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errore download statistiche Tracciato: " + ex.Message);
                return null;
            }

        }
    }

    public class Race
    {
        public string season { get; set; }
        public string round { get; set; }//uguale a nTurn di Circuit
        public string url { get; set; }
        public string raceName { get; set; }
        public Circuit Circuit { get; set; }
        public string date { get; set; }
        public string time
        {
            get
            {
                switch(Circuit.circuitId)
                {
                    case "albert_park":
                    case "suzuka":
                        return "05:10:00Z";

                    case "bahrain": return "15:10:00Z";
                    case "shanghai": return "06:10:00Z";
                    case "BAK": return "12:10:00Z";

                    case "catalunya":
                    case "monaco":
                    case "ricard":
                    case "red_bull_ring":
                    case "silverstone":
                    case "hockenheimring":
                    case "hungaroring":
                    case "spa":
                    case "monza":
                        return "13:10:00Z";

                    case "villeneuve": return "19:10:00Z";
                    case "marina_bay": return "12:10:00Z";
                    case "yas_marina": return "13:10:00Z";
                    case "sochi": return "11:10:00Z";

                    case "rodriguez":
                    case "americas":
                        return "19:10:00Z";
                 
                    case "interlagos": return "18:10:00Z";
                    default: return "";
                }
            }
                
        }
        public string timeUTZ { get { return date + "T" + time; } }
        public List<Result> Results { get; set; }
        public List<QualifyingResult> QualifyingResults { get; set; }
        public List<Lap> Laps { get; set; }

        public SolidColorBrush completato
        {
            get
            {
                DateTime adesso = DateTime.Now;
                DateTime gara = DateTime.Parse(timeUTZ, null, System.Globalization.DateTimeStyles.RoundtripKind);
                DateTime gara2Ore = gara.AddHours(2);

                if (adesso >= gara && adesso <= gara2Ore)//Se la gara è in corso
                {
                    return new SolidColorBrush(Windows.UI.Colors.Orange);
                }

                if (DateTime.Compare(adesso, gara) == 1)//Se la gara è finita
                {
                    return new SolidColorBrush(Windows.UI.Colors.Green);
                }
                else//Se la gara deve ancora farla
                {
                    return new SolidColorBrush(Windows.UI.Colors.Red);
                }
            }
        }

        public bool isComplete()
        {
            DateTime adesso = DateTime.Now;
            DateTime gara = DateTime.Parse(timeUTZ, null, System.Globalization.DateTimeStyles.RoundtripKind);

            return (DateTime.Compare(adesso, gara) == 1);
        }

        public string getLocalTime()
        {
            DateTime d2 = DateTime.Parse(timeUTZ, null, System.Globalization.DateTimeStyles.RoundtripKind);
            return d2.ToLocalTime().ToString().Replace('.',':');
        }

    }

    public class QualifyingResult
    {
        public string number { get; set; }
        public string position { get; set; }
        public Driver Driver { get; set; }
        public Constructor Constructor { get; set; }
        public string Q1 { get; set; }
        public string Q2 { get; set; }
        public string Q3 { get; set; }
    }

    public class Timing
    {
        public string driverId { get; set; }
        public string position { get; set; }
        public string time { get; set; }
        public int millisecondTime { get { return TimingUtils.timeToMillisecond(time); } }
    }

    public class Lap
    {
        public string number { get; set; }
        public List<Timing> Timings { get; set; }
    }


    public class RaceTable
    {
        public string season { get; set; }
        public string round { get; set; }
        public List<Race> Races { get; set; }
    }

    public class CircuitParent
    {
        public MRData MRData { get; set; }
    }
}
