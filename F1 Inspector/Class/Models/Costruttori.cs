﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace F1_Inspector.Class
{
    public class Constructor
    {
        public string constructorId { get; set; }
        public string url { get; set; }
        public string URLStats
        {
            get
            {
                if(!name.Contains("Haas"))
                    return "https://www.formula1.com/en/teams/" + UppercaseFirst(name.Trim()).Replace(" ","-") + ".html";
                else
                    return "https://www.formula1.com/en/teams/Haas.html";
            }
        }
        public string name { get; set; }
        public string nationality { get; set; }
        public string ImageCar { get { return string.Format("ms-appx:///Assets/img/Constructor/Car/{0}.png", constructorId); } }
        public string Logo
        {
            get
            {
                switch (constructorId)
                {
                    case "ferrari": return "https://www.formula1.com/content/fom-website/en/championship/teams/Ferrari/_jcr_content/logo.img.jpg/1521797345005.jpg";
                    case "mercedes": return "https://www.formula1.com/content/fom-website/en/championship/teams/Mercedes/_jcr_content/logo.img.jpg/1486740144183.jpg";
                    case "red_bull": return "https://www.formula1.com/content/fom-website/en/championship/teams/Red-Bull/_jcr_content/logo.img.jpg/1514762875081.jpg";
                    case "renault": return "https://www.formula1.com/content/fom-website/en/championship/teams/Renault/_jcr_content/logo.img.jpg/1509095937427.jpg";
                    case "haas": return "https://www.formula1.com/content/fom-website/en/championship/teams/Haas/_jcr_content/logo.img.jpg/1496929111587.jpg";
                    case "racing_point": return "https://www.formula1.com/content/fom-website/en/championship/teams/Force-India/_jcr_content/logo.img.jpg/1496922112802.jpg";
                    case "mclaren": return "https://www.formula1.com/content/fom-website/en/championship/teams/McLaren/_jcr_content/logo.img.jpg/1515152671364.jpg";
                    case "toro_rosso": return "https://www.formula1.com/content/fom-website/en/championship/teams/Toro-Rosso/_jcr_content/logo.img.jpg/1521797337296.jpg";
                    case "alfa": return "https://www.formula1.com/content/fom-website/en/championship/teams/Sauber/_jcr_content/logo.img.jpg/1516986071412.jpg";
                    case "williams": return "https://www.formula1.com/content/fom-website/en/championship/teams/Williams/_jcr_content/logo.img.jpg/1496922875259.jpg";
                    default: return null;
                }
            }
        }
        public SolidColorBrush colorConstructor { get { return getColorConstructor(constructorId); } }

        public static SolidColorBrush getColorConstructor(string constructorId)
        {
            var theme = Application.Current.RequestedTheme;

            switch (constructorId)
            {
                case "ferrari":
                case "Ferrari" :
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 220, 0, 0));
                case "mercedes":
                case "Mercedes":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 210, 190));
                case "red_bull":
                case "Red Bull":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 30, 65, 255));
                case "renault":
                case "Renault":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 245, 0));
                case "haas":
                case "Haas":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 189, 158, 87));
                case "racing_point":
                case "Racing Point":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 245, 150, 200));
                case "mclaren":
                case "McLaren":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 135, 0));
                case "toro_rosso":
                case "Toro Rosso":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 70, 155, 255));
                case "alfa":
                case "Alfa Romeo":
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 155, 0, 0));
                case "williams":
                case "Williams":
                    return theme == ApplicationTheme.Light ? new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0)) : new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
                default: return new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            }
        }
        
        

        public async Task<Dictionary<string, string>> Statistiche()
        {
            try
            {
                HttpClient client = new HttpClient();
                var url = new Uri(URLStats);
                var response = await client.GetStringAsync(url);

                var doc = new HtmlDocument();
                doc.LoadHtml(response);

                var chiaveRAW = doc.DocumentNode.SelectNodes("//section[@class='stats']//div//table//tbody//tr//th//span");
                var valoreRAW = doc.DocumentNode.SelectNodes("//section[@class='stats']//div//table//tbody//tr//td");
                Dictionary<String, String> stats = new Dictionary<string, string>();
                for (int i = 0; i < chiaveRAW.Count; i++)
                {
                    stats.Add(chiaveRAW[i].InnerText.Trim(), valoreRAW[i].InnerText.Trim());
                }
                return stats;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errore download statistiche Costruttore: " + ex.Message);
                return null;
            }
            
        }

        public async Task<int> VittorieAnnoCorrente()
        {
            try
            {
                HttpClient client = new HttpClient();
                var url = new Uri(string.Format("https://ergast.com/api/f1/2019/constructors/{0}/constructorStandings.json",constructorId));
                var response = await client.GetStringAsync(url);

                ConstructorParent r = new ConstructorParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ConstructorParent;
                return Convert.ToInt32(r.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[0].wins);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errore download statistiche Costruttore: " + ex.Message);
                return -1;
            }
        }

        private string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }

    public class ConstructorTable
    {
        public string season { get; set; }
        public List<Constructor> Constructors { get; set; }
    }

    public class MRData
    {
        public string xmlns { get; set; }
        public string series { get; set; }
        public string url { get; set; }
        public string limit { get; set; }
        public string offset { get; set; }
        public string total { get; set; }
        public ConstructorTable ConstructorTable { get; set; }
        public StandingsTable StandingsTable { get; set; }
        public RaceTable RaceTable { get; set; }
        public DriverTable DriverTable { get; set; }
    }

    public class ConstructorStanding
    {
        public string position { get; set; }
        public string positionText { get; set; }
        public string points { get; set; }
        public string wins { get; set; }
        public Constructor Constructor { get; set; }
    }

    public class StandingsList
    {
        public string season { get; set; }
        public string round { get; set; }
        public List<ConstructorStanding> ConstructorStandings { get; set; }
        public List<DriverStanding> DriverStandings { get; set; }
    }

    public class StandingsTable
    {
        public string season { get; set; }
        public string constructorId { get; set; }
        public List<StandingsList> StandingsLists { get; set; }
    }

    public class ConstructorParent
    {
        public MRData MRData { get; set; }
    }
}
