﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources;
using Windows.Storage;
using Windows.UI.Popups;

namespace F1_Inspector.Class
{
    class GetData
    {
        HttpClient client;
        ResourceLoader resourceLoader;
        public static string Anno = "2019";

        public GetData()
        {
            client = new HttpClient();
            resourceLoader = ResourceLoader.GetForCurrentView();
        }

        public async Task<List<Driver>> getPiloti()
        {
            try
            {
                var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/drivers.json",Anno));
                var response = await client.GetStringAsync(uri);
                DriverParent r = new DriverParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as DriverParent;
                return r.MRData.DriverTable.Drivers;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Constructor>> getCostruttori()
        {
            try
            {
                var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/constructors.json",Anno));
                var response = await client.GetStringAsync(uri);
                ConstructorParent r = new ConstructorParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ConstructorParent;
                return r.MRData.ConstructorTable.Constructors;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Race>> getCircuiti()
        {
            try
            {
                var uri = new Uri(string.Format("https://ergast.com/api/f1/{0}.json",Anno));
                var response = await client.GetStringAsync(uri);
                CircuitParent r = new CircuitParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as CircuitParent;
                return r.MRData.RaceTable.Races;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<DriverStanding>> getClassificaPiloti()
        {
            try
            {
                var uri = new Uri(string.Format("https://ergast.com/api/f1/{0}/driverStandings.json",Anno));
                var response = await client.GetStringAsync(uri);
                ClassificaParent r = new ClassificaParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ClassificaParent;

                return r.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<ConstructorStanding>> getClassificaCostruttori()
        {
            try
            {
                var uri = new Uri(string.Format("https://ergast.com/api/f1/{0}/constructorStandings.json", Anno));
                var response = await client.GetStringAsync(uri);
                ClassificaParent r = new ClassificaParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ClassificaParent;

                return r.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<Race> getNextRace()
        {
            try
            {
                var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/next.json", Anno));
                var response = await client.GetStringAsync(uri);
                CircuitParent r = new CircuitParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as CircuitParent;
                return r.MRData.RaceTable.Races[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<RaceTable> getResultRace(int round)
        {
            try
            {
                var uri = new Uri(string.Format("https://ergast.com/api/f1/{0}/{1}/results.json",Anno,round));
                var response = await client.GetStringAsync(uri);
                ClassificaParent r = new ClassificaParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ClassificaParent;

                string runtimeLanguages = Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().QualifierValues["Language"];
                if(!runtimeLanguages.Contains("en"))
                {
                    Traduttore tr = new Traduttore();
                    bool stop = false;
                    for (int j = 0; j < r.MRData.RaceTable.Races[0].Results.Count && !stop; j++)
                    {
                        if (r.MRData.RaceTable.Races[0].Results[j].status != "Finished")
                        {
                            string parola = await tr.TranslateToITA(r.MRData.RaceTable.Races[0].Results[j].status);
                            if(parola == null)
                            {
                                stop = true;
                                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
                            }
                            else
                                r.MRData.RaceTable.Races[0].Results[j].status = parola;
                        }
                            
                    }
                }
                
                return r.MRData.RaceTable;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Race>> getResultRacesDriver(string driver)
        {
            try
            {
                var uri = new Uri(string.Format("https://ergast.com/api/f1/{0}/drivers/{1}/results.json", Anno, driver));
                var response = await client.GetStringAsync(uri);
                ClassificaParent r = new ClassificaParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as ClassificaParent;

                string runtimeLanguages = Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().QualifierValues["Language"];
                if (!runtimeLanguages.Contains("en"))
                {
                    Traduttore tr = new Traduttore();
                    bool stop = false;
                    for (int j = 0; j < r.MRData.RaceTable.Races[0].Results.Count && !stop; j++)
                    {
                        if (r.MRData.RaceTable.Races[0].Results[j].status != "Finished")
                        {
                            string parola = await tr.TranslateToITA(r.MRData.RaceTable.Races[0].Results[j].status);
                            if (parola == null)
                            {
                                stop = true;
                                var m = new MessageDialog(resourceLoader.GetString("EIC")).ShowAsync();
                            }
                            else
                                r.MRData.RaceTable.Races[0].Results[j].status = parola;
                        }
                    }
                }

                return r.MRData.RaceTable.Races;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<QualifyingResult>> getResultQualifying(int round)
        {
            try
            {
                var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/{1}/qualifying.json", Anno, round));
                var response = await client.GetStringAsync(uri);
                QualifyingParent r = new QualifyingParent();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
                r = ser.ReadObject(ms) as QualifyingParent;

                return r.MRData.RaceTable.Races[0].QualifyingResults;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Lap>> getGraficoGara(Driver driver,Circuit circuit,string anno="")
        {
            try
            {
                if (anno == "")
                    anno = Anno;

                bool filePresente = await DownloadDataAndSave.IsFilePresent(circuit.circuitId, driver.driverId, anno);
                bool lavoro_completato = false;
                string response="";

                if (filePresente)
                {
                    var file = await DownloadDataAndSave.ReturnFile(circuit.circuitId, driver.driverId, anno);
                    if (file != null)
                    {
                        response = await FileIO.ReadTextAsync(file);
                        Debug.WriteLine(string.Format("File {0}.json già in locale",driver.driverId));
                        lavoro_completato = true;
                    }
                }
                if(!lavoro_completato)
                {
                    //Download singolo file
                    var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/{1}/drivers/{2}/laps.json?limit=100", anno, circuit.nTurn, driver.driverId));
                    Debug.WriteLine("Sto cercando di scaricare: " + uri.AbsoluteUri);
                    response = await client.GetStringAsync(uri);
                    DownloadDataAndSave.SaveDatiGPDriver(circuit, anno, driver, response);
                    Debug.WriteLine(string.Format("File {0}.json salvato adesso", driver.driverId));
                    lavoro_completato = true;
                }

                if (lavoro_completato)
                {
                    var result = getGraficoGaraLap(response);//.GetRange(0,2);//TODO rimuovi getRange
                    if (result != null)
                        return result;
                    else
                    {
                        Debug.WriteLine(string.Format("Il file {0}.json non contiene dati utili", driver.driverId));
                        DownloadDataAndSave.DeleteFile(circuit.circuitId, driver.driverId, anno);
                        return null;
                    }
                }
                else
                    return null;
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public List<Lap> getGraficoGaraLap(string response)
        {
            CircuitParent r = new CircuitParent();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(response));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(r.GetType());
            r = ser.ReadObject(ms) as CircuitParent;
            if (r.MRData.RaceTable.Races.Count > 0)
                return r.MRData.RaceTable.Races[0].Laps;
            else
                return null;
        }
    }
}
