﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace F1_Inspector.Class
{
    class DownloadDataAndSaveExperimental
    {
        /*private static CancellationTokenSource cancellationToken;
        private static DownloadOperation downloadOperation = null;
        public static async Task DownloadDatiGP2(Circuit circuito, string anno, TextBlock info)
        {
            try
            {
                BackgroundDownloader backgroundDownloader = new BackgroundDownloader();
                

                //Variabili di comunicazione
                GetData gd = new GetData();

                //Variabili utili
                StorageFolder rootFolder = ApplicationData.Current.LocalFolder;


                var piloti = await gd.getPiloti();

                foreach (var pilota in piloti)
                {
                    var uri = new Uri(string.Format("http://ergast.com/api/f1/2019/{0}/drivers/{1}/laps.json?limit=100", circuito.nTurn, pilota.driverId));
                    Debug.WriteLine(string.Format("Download Giri di {0}, url: {1}", pilota.familyName, uri.AbsoluteUri));

                    var file = await CreateFile(rootFolder, circuito.circuitId, pilota.driverId, anno);

                    downloadOperation = backgroundDownloader.CreateDownload(uri, file);
                    Progress<DownloadOperation> progress = new Progress<DownloadOperation>(x => ProgressChanged(downloadOperation,info));
                    cancellationToken = new CancellationTokenSource();
                    try
                    {
                        await downloadOperation.StartAsync().AsTask(cancellationToken.Token, progress);
                    }
                    catch (TaskCanceledException)
                    {
                        info.Text = "Download canceled.";
                        await downloadOperation.ResultFile.DeleteAsync();
                        downloadOperation = null;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("ERRORE DOWNLOAD FILES: " + e.Message);
                info.Text = "-1";
            }
        }

        private static void ProgressChanged(DownloadOperation downloadOperation,TextBlock TextBlockStatus)
        {
            int progress = (int)(100 * ((double)downloadOperation.Progress.BytesReceived / (double)downloadOperation.Progress.TotalBytesToReceive));

            switch (downloadOperation.Progress.Status)
            {
                case BackgroundTransferStatus.Running:
                case BackgroundTransferStatus.Completed:
                    {
                        TextBlockStatus.Text = string.Format("{0} of {1} kb. downloaded - {2}% complete.", downloadOperation.Progress.BytesReceived / 1024, downloadOperation.Progress.TotalBytesToReceive / 1024, progress);
                        //TextBlockStatus.Text = "Downloading...";
                        break;
                    }
                case BackgroundTransferStatus.PausedByApplication:
                    {
                        TextBlockStatus.Text = "Download paused.";
                        break;
                    }
                case BackgroundTransferStatus.PausedCostedNetwork:
                    {
                        TextBlockStatus.Text = "Download paused because of metered connection.";
                        break;
                    }
                case BackgroundTransferStatus.PausedNoNetwork:
                    {
                        TextBlockStatus.Text = "No network detected. Please check your internet connection.";
                        break;
                    }
                case BackgroundTransferStatus.Error:
                    {
                        TextBlockStatus.Text = "An error occured while downloading.";
                        break;
                    }
                case BackgroundTransferStatus.Canceled:
                    {
                        TextBlockStatus.Text = "Download canceled.";
                        break;
                    }
            }

            if (progress >= 100)
            {
                downloadOperation = null;
            }
        }

        private static async void SaveFile(StorageFolder rootFolder, string content, string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await rootFolder.CreateFolderAsync(anno, CreationCollisionOption.OpenIfExists);
                StorageFolder subFolder = await folder.CreateFolderAsync(circuitID, CreationCollisionOption.OpenIfExists);

                StorageFile fileJson = await subFolder.CreateFileAsync(pilotaID + ".json", CreationCollisionOption.OpenIfExists);

                await FileIO.WriteTextAsync(fileJson, content);
            }
            catch(Exception e)
            {
                Debug.WriteLine("SaveFileError: " + e.Message);
            }
        }

        private static async Task<StorageFile> CreateFile(StorageFolder rootFolder, string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await rootFolder.CreateFolderAsync(anno, CreationCollisionOption.OpenIfExists);
                StorageFolder subFolder = await folder.CreateFolderAsync(circuitID, CreationCollisionOption.OpenIfExists);

                StorageFile fileJson = await subFolder.CreateFileAsync(pilotaID + ".json", CreationCollisionOption.ReplaceExisting);

                return fileJson;
            }
            catch (Exception e)
            {
                Debug.WriteLine("CreateFileError: " + e.Message);
                return null;
            }
        }



        private async Task DownloadFile(string url)
        {
            using (var client = new WebClient())
            {
                string s = await client.DownloadStringTaskAsync(url);

            }
        }

        private async void Button_OnClick()
        {
            await DownloadFiles(new List<string>() { "ciao", "blue" });
        }

        private async Task DownloadFiles(List<string> urlList)
        {
            foreach (var url in urlList)
            {
                await DownloadFile(url);
            }
        }*/
    }
}
