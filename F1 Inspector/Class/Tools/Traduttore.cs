﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace F1_Inspector.Class
{
    public class Traduttore
    {
        private const string host = "https://translate.googleapis.com/translate_a/single?client=gtx";
        private const string params_ = "&sl=en&tl=it&dt=t";
        private const string uri = host + params_;

        private Dictionary<string, string> dizionario;
        ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
        bool visualizzaDizionario = true;

        public Traduttore()
        {
            AggiornaDizionario();
            visualizzaDizionario = false;
        }

        private void AggiornaDizionario()
        {
            if (localSettings.Values["dizionario"] != null)
            {
                var supp1 = localSettings.Values["dizionario"].ToString().Substring(0, localSettings.Values["dizionario"].ToString().Length - 1).Split('@');
                var lingua = supp1[0];
                var supp2 = supp1[1].Split(';');
                dizionario = new Dictionary<string, string>();

                Debug.WriteLineIf(visualizzaDizionario, "Dizionario:");
                for (int i = 0; i < supp2.Length; i++)
                {
                    var supp3 = supp2[i].Split(':');
                    Debug.WriteLineIf(visualizzaDizionario, string.Format("{0} -> {1}", supp3[0], supp3[1]));
                    dizionario.Add(supp3[0], supp3[1]);
                }
            }
        }

        public async Task<string> TranslateToITA(string text)
        {
            try
            {
                if (dizionario.ContainsKey(text))
                {
                    return dizionario[text];//Parola trovata in locale
                }
                else
                {
                    string urlContext = uri + "&q=" + Uri.EscapeUriString(text);

                    HttpClient client = new HttpClient();
                    string response = await client.GetStringAsync(urlContext);

                    string parola = response.Substring(4);
                    parola = parola.Substring(0, parola.IndexOf('\"'));
                    //Prima lettera maiuscola
                    parola = parola[0].ToString().ToUpper() + parola.Substring(1);

                    //+1 Lap -> 1 giro
                    if (text[0] == '+' && text[0] != parola[0])
                        parola = "+" + parola;

                    //Salvo la parola per le volte successive
                    AggiungiParola(text, parola);

                    return parola;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        private void AggiungiParola(string paroleING, string parola)
        {
            //ITA@Home:Casa;Dog:Cane;
            if (localSettings.Values["dizionario"] != null)
            {
                string nuovaTraduzione = paroleING + ":" + parola + ";";

                string parole = localSettings.Values["dizionario"].ToString();
                parole += nuovaTraduzione;
                localSettings.Values["dizionario"] = parole;
                
                AggiornaDizionario();
                Debug.WriteLine(string.Format("Parola inserita nel vocabolario: {0} -> {1}", paroleING, parola));
            }
        }
    }
}
