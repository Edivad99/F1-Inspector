﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Notifications;
using Microsoft.Toolkit.Uwp.Notifications; // Notifications library
using Microsoft.QueryStringDotNET; // QueryString.NET
using Windows.ApplicationModel;
using System.Net;

namespace F1_Inspector.Class
{
    class DownloadDataAndSave
    {
        static ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView();
        private static GetData gd = new GetData();
        private static StorageFolder rootFolder = ApplicationData.Current.LocalFolder;
        /// <summary>
        /// Scarica i dati dei tempi sul giro per ogni pilota
        /// </summary>
        public static async Task DownloadDatiGP(Circuit circuito, string anno)
        {
            try
            {
                var piloti = await gd.getPiloti();
                var pilotiDownload = new List<Driver>();

                foreach (var pilota in piloti)
                {
                    if (!(await IsFilePresent(circuito.circuitId, pilota.driverId, anno)))
                    {
                        pilotiDownload.Add(pilota);
                    }
                    else
                    {
                        Debug.WriteLine(string.Format("File {0}.json già scaricato", pilota.driverId));

                        var file = await ReturnFile(circuito.circuitId, pilota.driverId, anno);//controllo se ho salvato un file che non contiene dati di gara
                        if (file != null)
                        {
                            string response = await FileIO.ReadTextAsync(file);
                            var result = gd.getGraficoGaraLap(response);
                            if (result == null)
                            {
                                DeleteFile(circuito.circuitId, pilota.driverId, anno);
                                pilotiDownload.Add(pilota);
                            }
                        }
                    }
                }

                await DownloadFiles(pilotiDownload, anno, circuito);

                if (pilotiDownload.Count > 0)
                    SendNotification(Package.Current.DisplayName, resourceLoader.GetString("dwnCompletato"));

            }
            catch (Exception e)
            {
                Debug.WriteLine("ERRORE DOWNLOAD FILES: " + e.Message);
                var m = new MessageDialog(resourceLoader.GetString("DWD")).ShowAsync();
            }
        }

        public static void SaveDatiGPDriver(Circuit circuito, string anno, Driver pilota, string content)
        {
            try
            {
                //Variabili utili
                StorageFolder rootFolder = ApplicationData.Current.LocalFolder;
                Debug.WriteLine("Salvo Giri di " + pilota.familyName);
                SaveFile(rootFolder, content, circuito.circuitId, pilota.driverId, anno);
            }
            catch (Exception e)
            {
                Debug.WriteLine("ERRORE DOWNLOAD FILES: " + e.Message);
            }

        }

        private static async void SaveFile(StorageFolder rootFolder, string content, string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await rootFolder.CreateFolderAsync(anno, CreationCollisionOption.OpenIfExists);
                StorageFolder subFolder = await folder.CreateFolderAsync(circuitID, CreationCollisionOption.OpenIfExists);

                StorageFile fileJson = await subFolder.CreateFileAsync(pilotaID + ".json", CreationCollisionOption.ReplaceExisting);

                await FileIO.WriteTextAsync(fileJson, content);
            }
            catch(Exception e)
            {
                Debug.WriteLine("SaveFileError: " + e.Message);
            }
        }

        private static async Task<StorageFile> CreateFile(StorageFolder rootFolder, string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await rootFolder.CreateFolderAsync(anno, CreationCollisionOption.OpenIfExists);
                StorageFolder subFolder = await folder.CreateFolderAsync(circuitID, CreationCollisionOption.OpenIfExists);

                StorageFile fileJson = await subFolder.CreateFileAsync(pilotaID + ".json", CreationCollisionOption.ReplaceExisting);

                return fileJson;
            }
            catch (Exception e)
            {
                Debug.WriteLine("CreateFileError: " + e.Message);
                return null;
            }
        }

        public static async Task<bool> IsFilePresent(string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await ApplicationData.Current.LocalFolder.GetFolderAsync(anno);
                StorageFolder subFolder = await folder.GetFolderAsync(circuitID);

                var item = await subFolder.TryGetItemAsync(pilotaID + ".json");
                return item != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<StorageFile> ReturnFile(string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await ApplicationData.Current.LocalFolder.GetFolderAsync(anno);
                StorageFolder subFolder = await folder.GetFolderAsync(circuitID);
                
                return await subFolder.GetFileAsync(pilotaID + ".json");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async void DeleteFile(string circuitID, string pilotaID, string anno)
        {
            try
            {
                StorageFolder folder = await ApplicationData.Current.LocalFolder.GetFolderAsync(anno);
                StorageFolder subFolder = await folder.GetFolderAsync(circuitID);

                var item = await subFolder.TryGetItemAsync(pilotaID + ".json");

                await item.DeleteAsync();
                Debug.WriteLine("Elimino " + pilotaID + ".json");
            }
            catch (Exception e )
            {
                Debug.WriteLine("Errore cancellazione file: " + e.Message);
            }
        }

        public static async void DeleteAll()
        {
            try
            {
                StorageFolder rootFolder = ApplicationData.Current.LocalFolder;
                foreach (var x in await rootFolder.GetFoldersAsync())
                {
                    await x.DeleteAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Errore cancellazione file: " + e.Message);
            }
        }

        public static void SendNotification(string Title, string Content)
        {
            ToastVisual visual = new ToastVisual()
            {
                BindingGeneric = new ToastBindingGeneric()
                {
                    Children =
                    {
                        new AdaptiveText()
                        {
                            Text = Title
                        },

                        new AdaptiveText()
                        {
                            Text = Content
                        }
                    }
                }
            };
            // Now we can construct the final toast content
            ToastContent toastContent = new ToastContent()
            {
                Visual = visual
            };
            // And create the toast notification
            var toast = new ToastNotification(toastContent.GetXml());

            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private static async Task DownloadFiles(List<Driver> drivers, string anno, Circuit circuito)
        {
            foreach (var driver in drivers)
            {
                var uri = new Uri(string.Format("http://ergast.com/api/f1/{0}/{1}/drivers/{2}/laps.json?limit=100", anno, circuito.nTurn, driver.driverId));
                await DownloadFile(uri, anno, circuito, driver);
            }
        }
        private static async Task DownloadFile(Uri url, string anno, Circuit circuit, Driver driver)
        {
            using (var client = new WebClient())
            {
                string response = await client.DownloadStringTaskAsync(url);
                var check = gd.getGraficoGaraLap(response);
                if (check != null)
                {
                    SaveFile(rootFolder, response, circuit.circuitId, driver.driverId, anno);
                    Debug.WriteLine(string.Format("{0} è stato scaricato", driver.fullName));
                }
            }
        }
    }
}
