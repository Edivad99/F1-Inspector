﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F1_Inspector.Class
{
    class TimingUtils
    {
        public static int timeToMillisecond(string lapTime)
        {
            var t = lapTime.Split(":");
            var st = t[1].Split(".");

            int minuti = Convert.ToInt32(t[0]) * 60;
            int secondi = Convert.ToInt32(st[0]);
            int millesimi = Convert.ToInt32(st[1]);
            int totale = (minuti + secondi) * 1000 + millesimi;
            return totale;
        }
        public static string millisecondToTime(double totale)
        {
            int secondi = Math.Abs(Convert.ToInt32(totale)) / 1000;
            int millesimi = Math.Abs(Convert.ToInt32(totale)) % 1000;
            int minuti = secondi / 60;
            secondi = secondi % 60;

            if (totale < 0)
                return string.Format("-{0}:{1}.{2}", minuti, secondi.ToString().PadLeft(2, '0'), millesimi.ToString().PadLeft(3, '0'));
            else
                return string.Format("{0}:{1}.{2}", minuti, secondi.ToString().PadLeft(2, '0'), millesimi.ToString().PadLeft(3, '0'));
        }
    }
}
