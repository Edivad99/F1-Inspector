﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Media.Imaging;

namespace F1_Inspector.Class
{
    public class Articolo
    {
        public string Titolo { get; }
        public Uri Url { get; }
        public BitmapImage Foto { get; }
        public string Autore { get; set; }
        public string Giornale { get; set; }
        public DateTime DataPubblicazione { get; set; }

        public Articolo(string Titolo, string Autore, string Foto, string Url)
        {
            this.Titolo = Titolo;
            this.Autore = Autore;
            this.Url = new Uri(Url, UriKind.Absolute);
            this.Foto = new BitmapImage
            {
                UriSource = new Uri(Foto, UriKind.Absolute)
            };
        }
    }

    public class F1GrandPrix : Articolo
    {
        public F1GrandPrix(string Titolo, string Foto, string Url, string Autore, DateTime DataPubblicazione) : base(Titolo, Autore, Foto, Url)
        {
            base.Giornale = "F1GrandPrix";
            base.DataPubblicazione = DataPubblicazione;
        }

        public F1GrandPrix(string Titolo, string Foto, string Url, string Autore) : base(Titolo, Autore, Foto, Url)
        {
            base.Giornale = "F1GrandPrix";
            base.DataPubblicazione = DateTime.Now;
        }
    }

    public class FormulaPassion : Articolo
    {
        public FormulaPassion(string Titolo, string Foto, string Url, DateTime DataPubblicazione) : base(Titolo, "FormulaPassion", Foto, Url)
        {
            base.Giornale = "FormulaPassion";
            base.DataPubblicazione = DataPubblicazione;
        }
    }

    public class MotorSport : Articolo
    {
        public MotorSport(string Titolo, string Foto, string Url) : base(Titolo, "Motorsport", Foto, Url)
        {
            base.Giornale = "Motorsport";
            base.DataPubblicazione = DateTime.Now;
        }
    }

    public class ArticoliParameter
    {
        public ObservableCollection<Articolo> lista;
        public int indice;

        public ArticoliParameter(ObservableCollection<Articolo> lista, int indice)
        {
            this.lista = lista;
            this.indice = indice;
        }
    }

    public class GestoreArticoli
    {
        HttpClient client;
        public GestoreArticoli()
        {
            client = new HttpClient();
        }

        public async Task<ObservableCollection<Articolo>> GetArticoliF1GrandPrix()
        {
            var url = new Uri("http://f1grandprix.motorionline.com/category/news-formula-1/");
            var responseArray = await client.GetByteArrayAsync(url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);
            ObservableCollection<Articolo> lista = new ObservableCollection<Articolo>();

            //Recupero l'ultimo articolo
            var ultimoArticoloRAW = doc.DocumentNode.SelectSingleNode("//div[@class='fullElement noAuthor noCategory']//div[@class='content-article']//header[@class='header-article']//h2[@class='title-article']//a");

            var utltimoArticoloTitolo = System.Net.WebUtility.HtmlDecode(ultimoArticoloRAW.InnerText).Trim();
            var ultimoArticoloImmagine = doc.DocumentNode.SelectSingleNode("//div[@class='fullElement noAuthor noCategory']//article//div//a//img[@class='notMobile']").Attributes["src"].Value;
            var ultimoArticoloUrl = ultimoArticoloRAW.Attributes["href"].Value;
            var ultimoArticoloAutore = doc.DocumentNode.SelectSingleNode("//div[@class='fullElement noAuthor noCategory']//div[@class='content-article']//div[@class='author-article']//a").InnerText;

            //recupero la data
            //var data = await ArticoliUtils.getDataFromArticleF1GrandprixAsync(ultimoArticoloUrl, 0);

            lista.Add(new F1GrandPrix(utltimoArticoloTitolo, ultimoArticoloImmagine, ultimoArticoloUrl, ultimoArticoloAutore));

            //Recupero gli altri articoli
            var ArticoliRAW = doc.DocumentNode.SelectNodes("//div[@class='rowCol2 noCategory noAuthor']//div[@class='content-article']//header[@class='header-article']//h2//a");
            var FotoRAW = doc.DocumentNode.SelectNodes("//div[@class='rowCol2 noCategory noAuthor']//article//div//a//img[@class='notMobile']");
            var AutoreRAW = doc.DocumentNode.SelectNodes("//div[@class='rowCol2 noCategory noAuthor']//div[@class='content-article']//div[@class='author-article']//a");

            //for (int i = 0; i < ArticoliRAW.Count; i++)ì
            for (int i = 0; i < 10; i++)
            {
                var titolo = System.Net.WebUtility.HtmlDecode(ArticoliRAW[i].InnerText).Trim();
                var immagine = FotoRAW[i].Attributes["src"].Value;
                var urlInside = ArticoliRAW[i].Attributes["href"].Value;
                var autore = AutoreRAW[i].InnerText;


                //var d = await ArticoliUtils.getDataFromArticleF1GrandprixAsync(urlInside, i);
                lista.Add(new F1GrandPrix(titolo, immagine, urlInside, autore));
            }

            return lista;
        }

        public async Task<ObservableCollection<Articolo>> GetArticoliFormulaPassion()
        {
            var url = new Uri("https://www.formulapassion.it/motorsport/formula-1");
            var responseArray = await client.GetByteArrayAsync(url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);
            ObservableCollection<Articolo> lista = new ObservableCollection<Articolo>();

            //Recupero l'ultimo articolo
            var ultimoArticoloRAW = doc.DocumentNode.SelectSingleNode("//section[@class='main-news']//div[@class='fp-module-thumb']//a");

            var utltimoArticoloTitolo = System.Net.WebUtility.HtmlDecode(ultimoArticoloRAW.Attributes["title"].Value).Trim();
            var ultimoArticoloImmagine = doc.DocumentNode.SelectSingleNode("//section[@class='main-news']//div[@class='fp-module-thumb']//a//img").Attributes["src"].Value;
            var ultimoArticoloUrl = ultimoArticoloRAW.Attributes["href"].Value;
            var ultimoArticoloDataRAW = doc.DocumentNode.SelectSingleNode("//div[@class='boxevi boxevi-light']//article//time[@class='entry-date published']").Attributes["datetime"].Value.Trim();
            var ultimoArticoloDateTime = DateTime.Parse(ultimoArticoloDataRAW, null, System.Globalization.DateTimeStyles.RoundtripKind);

            lista.Add(new FormulaPassion(utltimoArticoloTitolo, ultimoArticoloImmagine, ultimoArticoloUrl, ultimoArticoloDateTime));

            //Recupero gli altri articoli
            var ArticoliRAW = doc.DocumentNode.SelectNodes("//div[@class='boxevi boxevi-light']//h3[@class='entry-title']//a");
            var FotoRAW = doc.DocumentNode.SelectNodes("//div[@class='boxevi boxevi-light']//article//div[@class='fp-animation-stack fp-big-thumb']//div//a//img");
            var DataRAW = doc.DocumentNode.SelectNodes("//div[@class='boxevi boxevi-light']//article//time[@class='entry-date published']");

            for (int i = 0; i < ArticoliRAW.Count; i++)
            {
                var titolo = System.Net.WebUtility.HtmlDecode(ArticoliRAW[i].InnerText).Trim();
                var immagine = FotoRAW[i].Attributes["src"].Value;
                var urlInside = ArticoliRAW[i].Attributes["href"].Value;

                var data = DataRAW[i].Attributes["datetime"].Value.Trim();
                //2019-03-10T12:00:44+01:00
                var d = DateTime.Parse(data, null, System.Globalization.DateTimeStyles.RoundtripKind);

                lista.Add(new FormulaPassion(titolo, immagine, urlInside, d));
            }

            return lista;
        }

        public async Task<ObservableCollection<Articolo>> GetArticoliMotorSport()
        {
            var url = new Uri("https://it.motorsport.com/f1/news/");
            var responseArray = await client.GetByteArrayAsync(url);
            var response = Encoding.UTF8.GetString(responseArray, 0, responseArray.Length - 1);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);
            ObservableCollection<Articolo> lista = new ObservableCollection<Articolo>();

            //Recupero gli articoli
            var ArticoliRAW = doc.DocumentNode.SelectNodes("//div[@class='ms-item--art ']//div[@class='ms-item_info ms-item_info--with-footer']//h3//a");
            var FotoRAW = doc.DocumentNode.SelectNodes("//div[@class='ms-item--art ']//div[@class='ms-item_thumb']//a[@class='ms-item_link']//img");
            for (int i = 0; i < 10; i++)
            {
                var titolo = System.Net.WebUtility.HtmlDecode(ArticoliRAW[i].InnerText).Trim();
                var immagine = FotoRAW[i].Attributes["data-src"].Value;
                var urlInside = "https://it.motorsport.com" + ArticoliRAW[i].Attributes["href"].Value;

                lista.Add(new MotorSport(titolo, immagine, urlInside));
            }

            return lista;
        }

        public async Task<ObservableCollection<Articolo>> GetArticoliByName(string giornale)
        {
            switch(giornale)
            {
                case "F1GrandPrix": return await GetArticoliF1GrandPrix();
                case "FormulaPassion": return await GetArticoliFormulaPassion();
                case "Motorsport.com": return await GetArticoliMotorSport();
                default: return null;
            }
        }
    }

    public class ArticoliUtils
    {
        static ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView();
        public static int daMeseANumero(string Mese)
        {
            switch (Mese.ToLower())
            {
                case "gennaio": return 1;
                case "febbraio": return 2;
                case "marzo": return 3;
                case "aprile": return 4;
                case "maggio": return 5;
                case "giugno": return 6;
                case "luglio": return 7;
                case "agosto": return 8;
                case "settembre": return 9;
                case "ottobre": return 10;
                case "novembre": return 11;
                case "dicembre": return 12;
                default: return -1;
            }
        }

        public static async Task<DateTime> getDataFromArticleF1GrandprixAsync(string Url, int index)
        {
            HttpClient client = new HttpClient();
            //recupero la data
            var subResponseArray = await client.GetByteArrayAsync(new Uri(Url, UriKind.Absolute));
            var subResponse = Encoding.UTF8.GetString(subResponseArray, 0, subResponseArray.Length - 1);
            var subdoc = new HtmlDocument();
            subdoc.LoadHtml(subResponse);

            var dataRAW = subdoc.DocumentNode.SelectSingleNode("//div[@class='post-info']//span[@class='date']").InnerText.Trim().Replace(',', ' ').Split(' ');
            var d = new DateTime(int.Parse(dataRAW[3]), daMeseANumero(dataRAW[1]), int.Parse(dataRAW[0]));
            return d;
        }

        public static string calcoloTempoLettura(string text)
        {
            int wordCount = 0, index = 0;

            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && char.IsWhiteSpace(text[index]))
                    index++;
            }

            int minuti = wordCount / 200;

            if (minuti == 1)
                return minuti + " " + resourceLoader.GetString("minuto");
            else if (minuti == 0)
                return resourceLoader.GetString("menominuto");
            else
                return minuti.ToString() + " " + resourceLoader.GetString("minuti");
        }
    }
}
