﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.ViewManagement;
using F1_Inspector.Schermate;
using Windows.Foundation.Metadata;
using F1_Inspector.Schermate.Scuderie;
using F1_Inspector.Schermate.Circuiti;
using F1_Inspector.Schermate.Piloti;
using F1_Inspector.Class;
using System.Diagnostics;
using Windows.UI.Popups;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Media.Animation;
using Windows.System;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x410

namespace F1_Inspector
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void MyNavView_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {
            var resourceLoader = ResourceLoader.GetForCurrentView();
            if (args.IsSettingsSelected)
            {
                ContentFrame.Navigate(typeof(Impostazioni), null, new EntranceNavigationTransitionInfo());
                myNavView.Header = resourceLoader.GetString("HImpostazioni");
            }
            else
            {
                
                NavigationViewItem item = args.SelectedItem as NavigationViewItem;

                switch (item.Tag)
                {
                    case "Home":
                        ContentFrame.Navigate(typeof(Home), null, new EntranceNavigationTransitionInfo());
                        myNavView.AlwaysShowHeader = false;
                        break;
                    case "News":
                        ContentFrame.Navigate(typeof(News), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HNews");
                        break;
                    case "Classifica":
                        ContentFrame.Navigate(typeof(Classifiche), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HClassifica");
                        break;
                    case "Piloti":
                        ContentFrame.Navigate(typeof(Piloti), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HPiloti");
                        break;
                    case "Costruttori":
                        ContentFrame.Navigate(typeof(Scuderie), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HCostruttori");
                        break;
                    case "GranPremi":
                        ContentFrame.Navigate(typeof(Circuiti), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HGranPremi");
                        break;
                    case "Grafici":
                        ContentFrame.Navigate(typeof(Grafici), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HGrafici");
                        break;
                    case "Replay":
                        ContentFrame.Navigate(typeof(Replay), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HReplay");
                        break;
                    case "Replay2":
                        ContentFrame.Navigate(typeof(Replay2), null, new EntranceNavigationTransitionInfo());
                        myNavView.Header = resourceLoader.GetString("HReplay");
                        break;
                }
            }
            
        }

        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private void MyNavView_Loaded(object sender, RoutedEventArgs e)
        {
            myNavView.SelectedItem = myNavView.MenuItems[0];

            // Add handler for ContentFrame navigation.
            ContentFrame.Navigated += On_Navigated;

            // Add keyboard accelerators for backwards navigation.
            var goBack = new KeyboardAccelerator { Key = VirtualKey.GoBack };
            goBack.Invoked += BackInvoked;
            this.KeyboardAccelerators.Add(goBack);

            // ALT routes here
            var altLeft = new KeyboardAccelerator
            {
                Key = VirtualKey.Left,
                Modifiers = VirtualKeyModifiers.Menu
            };
            altLeft.Invoked += BackInvoked;
            this.KeyboardAccelerators.Add(altLeft);
        }

        private void BackInvoked(KeyboardAccelerator sender, KeyboardAcceleratorInvokedEventArgs args)
        {
            On_BackRequested();
            args.Handled = true;
        }

        private bool On_BackRequested()
        {
            if (!ContentFrame.CanGoBack)
                return false;

            ContentFrame.GoBack();
            return true;
        }

        private void On_Navigated(object sender, NavigationEventArgs e)
        {
            //myNavView.IsBackEnabled = ContentFrame.CanGoBack;
            var pagina = ContentFrame.CurrentSourcePageType.Name;
            if (pagina.Equals("CircuitiDetails") || pagina.Equals("PilotiDetails") || pagina.Equals("RisultatiStagionali") || pagina.Equals("RisultatiDetails") || pagina.Equals("ScuderieDetails") || pagina.Equals("NewsArticle"))
            {
                myNavView.IsBackEnabled = true;
                myNavView.IsBackButtonVisible = NavigationViewBackButtonVisible.Visible;
            }
            else
            {
                myNavView.IsBackEnabled = false;
                myNavView.IsBackButtonVisible = NavigationViewBackButtonVisible.Collapsed;
            }

            if(pagina.Equals("Grafici") || pagina.Equals("Grafici2") || pagina.Equals("Replay") || pagina.Contains("News") )
            {
                myNavView.PaneDisplayMode = NavigationViewPaneDisplayMode.LeftCompact;
            }
            else
                myNavView.PaneDisplayMode = NavigationViewPaneDisplayMode.Auto;

            //Show header
            myNavView.AlwaysShowHeader = true;
            if (pagina.Equals("Home") || pagina.Equals("NewsArticle"))
            {
                myNavView.AlwaysShowHeader = false;
            }
            Debug.WriteLine("Sei sulla pagina -> " + pagina);
        }

        private void MyNavView_BackRequested(NavigationView sender, NavigationViewBackRequestedEventArgs args)
        {
            On_BackRequested();
        }
    }
}
